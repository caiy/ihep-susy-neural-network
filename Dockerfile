FROM archlinux:latest

RUN pacman -Syu --noconfirm && \
    pacman -S --noconfirm openssh cmake git gcc python python-pip xrootd python-uproot python-tensorflow tensorflow python-keras python-scikit-learn python-matplotlib python-tabulate && \
    pacman -Scc --noconfirm
CMD bash
