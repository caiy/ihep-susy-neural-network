import utils
import matplotlib.pyplot as plt
import file_handler
import numpy as np

W = file_handler.read_ntuple(".W.root", "NOMINAL", ["class0", "class1", "class2"])
W = file_handler.read_ntuple(".W.root", "NOMINAL", ["class0", "class1", "class2"])
W = file_handler.read_ntuple(".W.root", "NOMINAL", ["class0", "class1", "class2"])

W_np = W.to_numpy()

print(W_np[:, 0])
print(W_np[:, 1])
print(W_np[:, 2])

fig = plt.figure()
ax = fig.add_subplot(projection='3d')

ax.scatter(xs, ys, zs, marker='o')
ax.scatter(xs, ys, zs, marker='^')
ax.scatter(xs, ys, zs, marker='v')

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
