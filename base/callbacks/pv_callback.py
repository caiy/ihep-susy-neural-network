import os
from keras.callbacks import Callback
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn import metrics
from scipy import integrate


class PerformanceVisualizationCallback(Callback):

    def __init__(self, model, validation_data, image_dir):
        super().__init__()
        self.model = model
        self.validation_data = validation_data

        os.makedirs(image_dir, exist_ok=True)
        self.image_dir = image_dir

    def on_epoch_end(self, epoch, logs={}):
        class_num = self.validation_data[1].max() + 1
        predict = self.model.predict(self.validation_data[0])
        if class_num > 2:
            predict = tf.nn.softmax(predict)
            label = to_categorical(self.validation_data[1])
        else:
            predict = tf.math.sigmoid(predict)
            label = self.validation_data[1]
        #weight = self.validation_data[0][self.model.weight_feature]
        weight = self.validation_data[2]

        fpr = dict()
        tpr = dict()
        auc_value = dict()
        if class_num > 2:
            for i in range(class_num):
                fpr[i], tpr[i], _ = metrics.roc_curve(label[:, i], predict[:, i], sample_weight=weight)
                sorted_index = np.argsort(fpr[i])
                fpr_list_sorted = np.array(fpr[i])[sorted_index]
                tpr_list_sorted = np.array(tpr[i])[sorted_index]
                auc_value[i] = integrate.trapz(y=tpr_list_sorted, x=fpr_list_sorted)
                #auc_value[i] = metrics.auc(fpr[i], tpr[i])
        else:
            fpr, tpr, _ = metrics.roc_curve(label, predict, sample_weight=weight)
            sorted_index = np.argsort(fpr)
            fpr_list_sorted = np.array(fpr)[sorted_index]
            tpr_list_sorted = np.array(tpr)[sorted_index]
            auc_value = integrate.trapz(y=tpr_list_sorted, x=fpr_list_sorted)
            #auc_value = metrics.auc(fpr, tpr)

        plt.figure()
        plt.plot([0, 1], [0, 1], 'k--')
        if class_num > 2:
            for i in range(class_num):
                plt.plot(fpr[i], tpr[i], label='ROC curve of class {0} (AUC = {1:0.2f})'.format(i, auc_value[i]))
        else:
            plt.plot(fpr, tpr, label='ROC curve of class 1 (AUC = {0:0.2f})'.format(auc_value))

        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curve')
        plt.legend(loc='lower right')
        plt.savefig(self.image_dir + '/' + str(epoch) + "_ROC.png", dpi=300)
        plt.clf()
