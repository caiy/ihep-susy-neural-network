from tensorflow import keras
import matplotlib.pyplot as plt


class LossCallBack(keras.callbacks.Callback):

    def __init__(self):
        super().__init__()
        self.loss = None
        self.acc = None
        self.val_loss = None
        self.val_acc = None

    def on_train_begin(self, logs={}):
        self.loss = {'batch': [], 'epoch': []}
        self.acc = {'batch': [], 'epoch': []}
        self.val_loss = {'batch': [], 'epoch': []}
        self.val_acc = {'batch': [], 'epoch': []}

    def on_batch_end(self, batch, logs={}):
        self.loss['batch'].append(logs.get('loss'))
        self.acc['batch'].append(logs.get('accuracy'))
        self.val_loss['batch'].append(logs.get('val_loss'))
        self.val_acc['batch'].append(logs.get('val_accuracy'))

    def on_epoch_end(self, batch, logs={}):
        self.loss['epoch'].append(logs.get('loss'))
        self.acc['epoch'].append(logs.get('accuracy'))
        self.val_loss['epoch'].append(logs.get('val_loss'))
        self.val_acc['epoch'].append(logs.get('val_accuracy'))

    def loss_plot(self, loss_type):
        iters = range(len(self.loss[loss_type]))
        plt.figure()
        plt.plot(iters, self.acc[loss_type], 'r', label='train acc')
        if loss_type == 'epoch':
            plt.plot(iters, self.val_acc[loss_type], 'b', label='val acc')
        plt.xlabel(loss_type)
        plt.ylabel('accuracy')
        plt.legend(loc="upper right")
        plt.savefig("result_acc.png")

        plt.clf()
        plt.figure()
        plt.plot(iters, self.loss[loss_type], 'g', label='train loss')
        if loss_type == 'epoch':
            plt.plot(iters, self.val_loss[loss_type], 'k', label='val loss')
        plt.xlabel(loss_type)
        plt.ylabel('loss')
        plt.legend(loc="upper right")
        plt.savefig("result_loss.png")
