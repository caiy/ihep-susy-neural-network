import tensorflow as tf
import math


class lr_polynomial_decay:

    def __init__(self, epochs, initial_learning_rate, power=1.0):
        self.epochs = epochs
        self.lr = initial_learning_rate
        self.power = power

    def __call__(self, epoch):
        decay = (1 - (epoch / float(self.epochs)))**self.power
        updated_eta = float(self.lr * decay)
        # tf.summary.scalar('learning rate', data=updated_eta, step=epoch)
        return updated_eta


class lr_time_based_decay:

    def __init__(self, initial_learning_rate, epochs):
        self.decay = initial_learning_rate / epochs
        self.lr = initial_learning_rate

    def __call__(self, epoch):
        self.lr = self.lr * 1 / (1 + self.decay * epoch)
        # tf.summary.scalar('learning rate', data=self.lr, step=epoch)
        return self.lr


class lr_exp_decay:

    def __init__(self, initial_learning_rate, k=0.1, stop_factor=0.005):
        self.lr = initial_learning_rate
        self.k = k
        self.stop_lr = initial_learning_rate * stop_factor

    def __call__(self, epoch):
        ilr = max(self.lr * math.exp(-self.k * epoch), self.stop_lr)
        # tf.summary.scalar('learning rate', data=ilr, step=epoch)
        return ilr


class lr_step_based_decay:

    def __init__(self, initial_learning_rate, drop_rate=0.8, epochs_drop=10, stop_factor=0.005):
        self.lr = initial_learning_rate
        self.epochs_drop = epochs_drop
        self.drop_rate = drop_rate
        self.stop_lr = initial_learning_rate * stop_factor

    def __call__(self, epoch):
        ilr = max(self.lr * math.pow(self.drop_rate, math.floor(epoch / self.epochs_drop)), self.stop_lr)
        # tf.summary.scalar('learning rate', data=ilr, step=epoch)
        return ilr


class lr_square_root_decay:

    def __init__(self, initial_learning_rate):
        self.lr = initial_learning_rate

    def __call__(self, epoch):
        ilr = self.lr * pow(1 + epoch, -0.5)
        # tf.summary.scalar('learning rate', data=ilr, step=epoch)
        return ilr


decay_dict = {
    "polynomial_decay": lr_polynomial_decay,
    "time_based_decay": lr_time_based_decay,
    "exp_decay": lr_exp_decay,
    "step_based_decay": lr_step_based_decay,
    "square_root_decay": lr_square_root_decay
}


class WarmupScheduler:

    def __init__(self, decay_scheduler, warmup_epochs=10, **kwargs):
        self.decay_scheduler = decay_dict[decay_scheduler](**kwargs)
        self.warmup_epochs = warmup_epochs
        self.lr = self.decay_scheduler.lr
        self.decay_scheduler.lr = self.decay_scheduler.lr * warmup_epochs  # continous lr

    def __call__(self, epoch):
        if epoch <= warmup_epochs:
            # linear warmup
            # tf.summary.scalar('learning rate', data=self.lr * epoch, step=epoch)
            return self.lr * epoch
        else:
            return self.decay_scheduler(epoch - warmup_epochs)
