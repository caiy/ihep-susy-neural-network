import keras.activations
#import tensorflow_datasets as tfds
import tensorflow as tf
from keras import initializers, regularizers, constraints
import numpy as np
from keras.utils import tf_utils
from base.layer.activations import *


class RMSNormalization(tf.keras.layers.Layer):
    """
    RMS Norm(https://arxiv.org/pdf/1910.07467.pdf), copy and modify from keras layerNorm sourcecode,
    remove fused norm calculation part since I don't know how to implement it :)
    """

    def __init__(self, axis=-1, epsilon=1e-3, scale=True, gamma_initializer="ones", gamma_regularizer=None, gamma_constraint=None, **kwargs):
        super().__init__(**kwargs)

        if isinstance(axis, (list, tuple)):
            self.axis = list(axis)
        elif isinstance(axis, int):
            self.axis = axis
        else:
            raise TypeError("Expected an int or a list/tuple of ints for the "
                            "argument 'axis', but received: %r" % axis)

        self.epsilon = epsilon
        self.scale = scale
        self.gamma = None
        self.gamma_initializer = initializers.get(gamma_initializer)
        self.gamma_regularizer = regularizers.get(gamma_regularizer)
        self.gamma_constraint = constraints.get(gamma_constraint)

        self.supports_masking = True

    def build(self, input_shape):
        self.axis = tf_utils.validate_axis(self.axis, input_shape)
        input_shape = tf.TensorShape(input_shape)

        param_shape = [input_shape[dim] for dim in self.axis]
        if self.scale:
            self.gamma = self.add_weight(
                name="gamma",
                shape=param_shape,
                initializer=self.gamma_initializer,
                regularizer=self.gamma_regularizer,
                constraint=self.gamma_constraint,
                trainable=True,
                experimental_autocast=False,
            )
        self.built = True

    def call(self, inputs):
        inputs = tf.cast(inputs, self.compute_dtype)
        # Compute the axes along which to reduce the mean / variance
        input_shape = inputs.shape
        ndims = len(input_shape)

        # Broadcasting only necessary for norm when the axis is not just
        # the last dimension
        broadcast_shape = [1] * ndims
        for dim in self.axis:
            broadcast_shape[dim] = input_shape.dims[dim].value

        def _broadcast(v):
            if v is not None and len(v.shape) != ndims and self.axis != [ndims - 1]:
                return tf.reshape(v, broadcast_shape)
            return v

        input_dtype = inputs.dtype
        if input_dtype in ("float16", "bfloat16") and self.dtype == "float32":
            # If mixed precision is used, cast inputs to float32 so that
            # this is at least as numerically stable as the fused version.
            inputs = tf.cast(inputs, "float32")

        # Compute RMS normalization
        rms = tf.reduce_mean(tf.square(inputs), axis=[-1], keepdims=True)
        outputs = inputs * tf.math.rsqrt(rms + self.epsilon)
        if self.scale:
            scale = _broadcast(self.gamma)
            outputs = outputs * scale
        return tf.cast(outputs, input_dtype)

    def compute_output_shape(self, input_shape):
        return input_shape


# FFN with residual connection
class FeedForward(tf.keras.layers.Layer):

    # usually in transformer, the dim_hidden = input.shape[1] * input.shape[2]
    def __init__(self, dim_out, dim_hidden, activation='ge_glu', layer_norm=RMSNormalization(), dropout=0.1):
        super().__init__()
        hidden_activ = activation
        if activation == 'ge_glu':
            dim_hidden *= 2
            hidden_activ = ge_glu
        self.seq = tf.keras.Sequential(
            [tf.keras.layers.Dense(dim_hidden, activation=hidden_activ),
             tf.keras.layers.Dense(dim_out),
             tf.keras.layers.Dropout(dropout)])
        self.add = tf.keras.layers.Add()
        self.layer_norm = layer_norm

    def call(self, x):
        x = self.add([x, self.seq(x)])
        x = self.layer_norm(x)
        return x


# https://www.tensorflow.org/text/tutorials/transformer
class SelfAttention(tf.keras.layers.Layer):

    def __init__(self, num_head, dim_key, dropout=0, layer_norm=RMSNormalization(), use_item_mask=True, is_decoder=False, **kwargs):
        super().__init__()
        self.mha = tf.keras.layers.MultiHeadAttention(num_heads=num_head, key_dim=dim_key, dropout=dropout, **kwargs)
        self.add = tf.keras.layers.Add()
        self.layer_norm = layer_norm
        self.is_decoder = is_decoder
        self.use_item_mask = use_item_mask

    def call(self, x):
        # todo: Add attention mask for all zero items, here use 'and False' to disable it until this block is finished
        if self.use_item_mask and False:
            attention_mask = None
            attn_output = self.mha(query=x, value=x, key=x, use_causal_mask=self.is_decoder, attention_mask=attention_mask)
        else:
            attn_output = self.mha(query=x, value=x, key=x, use_causal_mask=self.is_decoder)
        x = self.add([x, attn_output])
        # It is better to move the norm layer into the following FC part. [2002.04745] [2003.07845]
        if self.layer_norm:
            x = self.layer_norm(x)
        return x


class TransformerEncoder(tf.keras.layers.Layer):
    """
    https://www.tensorflow.org/text/tutorials/transformer
    usually we only need the encoder and don't need the decoder/cross-attention. So here the cross-attention is not
    implemented and the decoder is implemented using the arg 'is_decoder'.
    Usually in transformer, the dim_ffn = input.shape[1] * input.shape[2]
    """

    def __init__(self,
                 num_head,
                 dim_key,
                 dim_out,
                 dim_ffn,
                 dropout=0.1,
                 ffn_activation='ge_glu',
                 layer_norm=RMSNormalization(),
                 use_item_mask=True,
                 is_decoder=False,
                 **kwargs):
        super().__init__()
        self.self_att = SelfAttention(num_head, dim_key, dropout, layer_norm, use_item_mask, is_decoder, **kwargs)
        self.ffn = FeedForward(dim_out, dim_ffn, dropout_rate=dropout, activation=ffn_activation, layer_norm=layer_norm)

    def call(self, x):
        x = self.self_att(x)
        x = self.ffn(x)
        return x
