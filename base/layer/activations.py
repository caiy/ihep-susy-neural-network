import keras.activations


def ge_glu(x, approximate=False):
    if x.shape[-1] % 2 != 0:
        raise ValueError("The shape of the input last dim should be a even value!")
    var, gate = tf.split(x, 2, axis=-1)
    return var * keras.activations.gelu(gate, approximate)
