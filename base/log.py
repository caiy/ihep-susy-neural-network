import logging

logger = logging.getLogger("logger")


def init_logger(workdir, verbose=False):
    """
    Initializes the logger with file and console handlers.

    This function configures a logger that writes log messages to both a file and the console. 
    The log file is saved in the directory specified by `workdir`, and the logging level can be set to DEBUG or INFO based on the `verbose` parameter.

    Args:
        workdir (str): The directory path (without file extension) where the log file will be created. 
        verbose (bool, optional): If True, sets the logging level to DEBUG, which provides detailed log messages for debugging purposes. 
                                  If False, sets the logging level to INFO, which provides general log messages. Defaults to False.
    """
    fh = logging.FileHandler(workdir + '.log')
    ch = logging.StreamHandler()
    formatter = logging.Formatter('[%(asctime)s|%(levelname)s] %(message)s')

    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    if verbose:
        logger.setLevel(logging.DEBUG)
        ch.setLevel(logging.DEBUG)
        fh.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)
        fh.setLevel(logging.INFO)
    logger.addHandler(ch)
    logger.addHandler(fh)
