# -*- coding: utf-8 -*-

import pandas as pd
from base.log import logger
import uproot
import re
import array
import numpy as np
import time
from base.utils import check_bad_data
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
import imblearn

def get_tree_list(file_name, regex_list):
    """
    Extracts and returns a sorted list of unique tree names from a ROOT file that match any of the provided regular expressions.

    Args:
        file_name (str): The name of the ROOT file to be opened and searched.
        regex_list (list of str): A list of regular expression patterns to match against the tree names in the ROOT file.

    Returns:
        matched_trees (list of str): A sorted list of unique tree names from the ROOT file that match any of the regular expressions provided in `regex_list`.
    """
    matched_trees = []
    data_file = uproot.open(file_name)
    all_trees = dict(data_file.items(filter_classname="TTree"))
    for i in range(len(regex_list)):
        reg_s = regex_list[i]
        reg = re.compile(reg_s)
        for tree_key in all_trees:
            results = reg.match(tree_key)
            if results is not None:
                matched_trees.append(tree_key.replace(';1', ''))
    # duplicate members removal
    matched_trees = list(set(matched_trees))
    matched_trees.sort()
    return matched_trees


class FileHandler:
    """
    A class to handle file operations based on the sample configurations.

    Attributes:
        input_file_name (str): The name of the input file.
        train_val_test_frac (list): Fractions for train, validation, and test splits.
        total_sample_frac (float): The total sample fraction.
        sample_labels (list): List of sample labels.
        random_seed (int): Random seed for reproducibility.
        branch_list (list): List of all features to be used.
        weight_branch (str): Branch to be used for weighting.
        max_weight (int): Maximum weight value.
        sampling_trees (dict): Dictionary to store sampling trees.
        # merged_pd (pd.DataFrame): A DataFrame to store merged data.
    """

    def __init__(self, input_file_name, sample_config):
        """
        Initialize the FileHandler with input file name and sample configuration.

        Args:
            input_file_name (str): The name of the input file.
            sample_config (dict): Dictionary containing sample configuration.

        Raises:
            RuntimeError: If the train_val_test_frac is not a list of three numbers.
        
        Notes:
            Mandatory keys in sample_config: train_val_test_frac, label_group, nominal_features, weight_branch
            Optional keys in sample_config: random_seed, max_weight, sampling
        """
        self.train_val_test_frac = sample_config['train_val_test_frac']
        self.total_sample_frac = 0
        if len(self.train_val_test_frac) != 3:
            logger.fatal('Sample Config Error: train, val, test frac should be exactly 3 numbers in a list!')
            raise RuntimeError('Sample config format error for key train_val_test_frac')
        else:
            self.total_sample_frac = sum(self.train_val_test_frac)
            if self.total_sample_frac != 1:
                self.train_val_test_frac = [i / self.total_sample_frac for i in self.train_val_test_frac]
                self.total_sample_frac = sum(self.train_val_test_frac)

        self.sample_labels = sample_config['label_group']
        self.random_seed = sample_config.get('random_seed', 12345)
        np.random.seed(self.random_seed)

        self.input_file_name = input_file_name
        self.branch_list = sample_config['nominal_features'].copy()
        self.weight_branch = sample_config['weight_branch']
        self.channel_branch = sample_config.get('channel_branch', "fb_mcChannelNumber")
        self.branch_list.append(self.weight_branch)
        self.branch_list.append(self.channel_branch)
        self.max_weight = sample_config.get('max_weight', -1)
        sampling_tree_regex = sample_config.get('sampling', {})
        self.sampling_trees = {}
        for k, v in sampling_tree_regex.items():
            trees = get_tree_list(input_file_name, [k])
            for t in trees:
                self.sampling_trees[t] = v
        self.over_sampling = sample_config.get('over_sampling', '')
        self.feature_list = sample_config['nominal_features']
        self.weight_handle = sample_config.get('weight_handle',None)

    def read_ntuple(self, file_name, tree_name, branch_list=[]):
        """
        Read branches of a tree in a root file, and return as a pd.DataFrame

        Args:
            file_name (str): The name of file to read.
            tree_name (str): The name of the tree to read from the file.
            branch_list (list, optional): List of branches to read. Defaults to []. If [], all branches are included.
        Returns:
            PDdata (pd.DataFrame)
        """
        with uproot.open(file_name) as root_file:
            logger.info("Reading %s in %s", tree_name, file_name)
            if tree_name:
                if not branch_list:
                    #PDdata = root_file[tree_name].arrays(library="pd")
                    PDdata = pd.DataFrame(root_file[tree_name].arrays(library="np")).astype(float)
                else:
                    #PDdata = root_file[tree_name].arrays(branch_list, library="pd")
                    PDdata = pd.DataFrame(root_file[tree_name].arrays(branch_list, library="np")).astype(float)
            else:
                raise RuntimeError('read_ntuple: tree name issue.')
                # PDdata = root_file
        return PDdata

    def get_all_sample_pd(self):  #, predict=False):
        """
        Read data of all samples, add lebal, TreeName(if predict), filter and sampling (if applicable).
        The label is 1 for signal and 0 for backgrounds in binary classification.

        Args:
            predict (bool, optional): Whether the data will be used for prediction. Defaults to False.
        Raises:
            RuntimeError: If the number of classes is less than 2.
        Returns:
            pd.DataFrame: DataFrame containing all sample data.
        Notes:
            If sampling_trees is available, sample the trees multiple times.
        """
        if len(self.sample_labels) < 2:
            raise RuntimeError('label size smaller than 2! Could not process any classification!')
        tic = time.perf_counter()
        label = 0
        total_list = []
        for sample_name in self.sample_labels:
            tree_list = get_tree_list(self.input_file_name, sample_name)
            data_list = []
            for tree in tree_list:
                tree_data = self.read_ntuple(self.input_file_name, tree, self.branch_list)
                tree_data = self.filter_weight(tree_data)
                tree_data['TreeName'] = tree
                data_list.append(tree_data)
            sample_data = pd.concat(data_list)
            sample_data['label'] = label
            total_list.append(sample_data)
            label += 1
        toc = time.perf_counter()
        all_sample_pd = pd.concat(total_list)
        logger.info("Read all trees cost %s s", toc - tic)
        return all_sample_pd

    def get_train_test(self, predict=False):
        """
        Split the data into training and test sets. The dataframe is shuffled.
        Args: predict (bool, optional): Whether for prediction. Defaults to False.
        Returns: two DataFrames: train_df, and test_df.
        """
        total_samples = self.get_all_sample_pd()
        if not predict:
            check_bad_data(total_samples)
            
            # Handle negative weights based on the specified method
            if self.weight_handle == 'drop_negative':
                # Remove events with negative weights
                total_samples = total_samples[total_samples[self.weight_branch] > 0]
            elif self.weight_handle == 'min_positive':
                # Replace negative weights with the minimum positive weight in the same label
                for label in total_samples['label'].unique():
                    label_mask = (total_samples['label'] == label)
                    positive_weights = total_samples.loc[label_mask & (total_samples[self.weight_branch] > 0), self.weight_branch]
                    min_positive = positive_weights.min()
                    neg_mask = label_mask & (total_samples[self.weight_branch] <= 0)
                    total_samples.loc[neg_mask, self.weight_branch] = min_positive
            elif self.weight_handle == 'softplus':
                # Apply Softplus transformation
                total_samples[self.weight_branch] = np.log(1 + np.exp(total_samples[self.weight_branch]))
            elif self.weight_handle == 'exponential':
                # Apply exponential transformation
                total_samples[self.weight_branch] = np.exp(total_samples[self.weight_branch])
            elif self.weight_handle is not None:
                logger.warning(f"Unknown weight handling method: {self.weight_handle}. No transformation applied.")
            
        train_df, test_df = train_test_split(total_samples, test_size=self.train_val_test_frac[2], random_state=self.random_seed)
        # sampling is not suggested
        if not predict and self.over_sampling:
            X = train_df[self.feature_list]
            Y = train_df["label"]
            if len(self.sample_labels) > 2:
                sampling_strategy={ i : train_df[train_df["label"] == i].shape[0] for i in range(len(self.sample_labels)) }
                bkg = sum(sampling_strategy.values()) - sampling_strategy[0] # TODO: hard code class_id=0
                sampling_strategy[0] = bkg
            else:
                sampling_strategy='minority'
            oversampler = getattr(imblearn.over_sampling, self.over_sampling)(random_state=self.random_seed, sampling_strategy=sampling_strategy)
            sampled_X, sampled_Y = oversampler.fit_resample(X,Y)
            train_df = pd.concat([sampled_X, pd.Series(sampled_Y, name='label'), pd.Series(np.ones_like(sampled_Y), name=self.weight_branch)], axis=1)
        elif not predict and self.sampling_trees != {}:
            sampled_sets = []
            for tree_name, df in train_df.groupby('TreeName'):
                if tree_name in self.sampling_trees and self.sampling_trees[tree_name] != 1:
                    fraction = self.sampling_trees[tree_name]
                    logger.info(f"Events from {tree_name} are sampled with {fraction} times.")
                    over_sampling = int(fraction)
                    under_sampling = fraction - over_sampling
                    sampled_subset = df.sample(frac=under_sampling, random_state=self.random_seed)
                    if over_sampling > 0:
                        sampled_subset = pd.concat([sampled_subset] + [df] * over_sampling, ignore_index=True)
                else:
                    sampled_subset = df
                sampled_sets.append(sampled_subset)
            train_df = pd.concat(sampled_sets, ignore_index=True)
            train_df = shuffle(train_df, random_state=self.random_seed)
        # print(train_df.shape, test_df.shape)
        return train_df, test_df

    def get_train_val_test(self, predict=False):
        """
        Split the data into training, validation and test sets. The dataframe is shuffled.
        Args: predict (bool, optional): Whether for prediction. Defaults to False.
        Returns: three DataFrames: train_df, validate_df, and test_df.
        """
        temp_df, test_df = self.get_train_test(predict)
        train_df, val_df = train_test_split(
            temp_df,
            train_size=self.train_val_test_frac[0] / (self.train_val_test_frac[0] + self.train_val_test_frac[1]),
            random_state=self.random_seed)
        # print(train_df.shape, val_df.shape, test_df.shape)
        return train_df, val_df, test_df

    def get_nfold(self):
        n_fold = round(self.train_val_test_frac[0] / self.train_val_test_frac[1]) + 1
        logger.info("%s-fold cross-validation", n_fold)
        return n_fold

    def get_nfold_train_test(self, predict=False):
        """
        Split the data into traning and test sets for n-fold cross-validation.
        
        Args:
            predict (bool, optional): Whether to include prediction data. Defaults to False.
        Returns:
            tuple: A tuple containing the number of folds (n_fold), train_df, and test_df.
        """
        train_df, test_df = self.get_train_test(predict)
        n_fold = round(self.train_val_test_frac[0] / self.train_val_test_frac[1]) + 1
        logger.info("Shuffle and split sample for %s-fold cross-validation", n_fold)
        return n_fold, train_df, test_df

    def filter_weight(self, data):
        """ Remove events with weight larger than self.max_weight if self.max_weight > 0 """
        if self.max_weight > 0:
            data = data.loc[(self.max_weight > data[self.weight_branch]) & (data[self.weight_branch] > -self.max_weight)]
        return data

    # def read_full_nutple(self, file_name):
    #     """Not used"""
    #     with uproot.open(file_name) as root_file:
    #         PDdata = root_file
    #     return PDdata

    # def ntuple_to_pd(self, ntuple, branch_list=[]):
    #     """Not used"""
    #     if branch_list:
    #         return ntuple.arrays(library="pd")
    #     if not branch_list:
    #         return ntuple.arrays(branch_list, library="pd")

    # def get_tree_samples(self):
    #     """ Not used"""
    #     sample_list = []
    #     for sample_name in self.sample_labels:
    #         tree_list = get_tree_list(self.input_file_name, sample_name)
    #         for tree in tree_list:
    #             tree_data = self.read_ntuple(self.input_file_name, tree, self.branch_list)
    #             sample_list.append(tree_data)
    #     return sample_list

    # def split_sample_pd(self, tree_data, frac_start=0, frac_end=1, copy=False):
    #     """Not used"""
    #     current_entries = len(tree_data)
    #     if copy:
    #         temp_pd = tree_data.copy()
    #     else:
    #         temp_pd = tree_data

    #     if frac_start >= frac_end:
    #         return None
    #     if frac_end > 1:
    #         frac_end = 1
    #     if frac_start < 0:
    #         frac_start = 0

    #     first_part = int(current_entries * frac_start)
    #     second_part = int(current_entries * frac_end)

    #     temp_pd = temp_pd.iloc[first_part:second_part, :]

    #     return temp_pd

    # def array2tree(self, outArray, outName, outTree):
    #     """Not used"""
    #     outArrayType = outName + '/F'
    #     outHolder = array.array('f', [0])
    #     outBranch = []

    #     arrayLen = outArray.shape[0]
    #     arrayDim = outArray.shape[1]

    #     for j in range(arrayDim):
    #         outBranch.append(outTree.Branch(outName + str(j), outHolder, outArrayType))

    #     for i in range(arrayLen):
    #         # outTree.GetEntry(i)
    #         for j in range(arrayDim):
    #             outHolder[0] = outArray[i][j]
    #             outBranch[j].Fill()

    #     outTree.SetEntries(-1)
    #     outTree.Write()

    # def sample_data(self, tree_data, sampling):
    #     """
    #     Sample the data from a tree.
    #     Args:
    #         tree_data (pd.DataFrame): DataFrame containing the tree data to be sampled.
    #         sampling (float): Fraction of the data to sample. Can be greater than 1 to duplicate data.
    #     Returns:
    #         pd.DataFrame: DataFrame containing the sampled data.
    #     """
    #     current_entries = len(tree_data)

    #     if sampling <= 0:
    #         return tree_data
    #     over_sampling = int(sampling)
    #     under_sampling = sampling - over_sampling
    #     temp_pd = tree_data.sample(frac=under_sampling, random_state=self.random_seed)
    #     if over_sampling > 0:
    #         temp_pd2 = pd.concat([tree_data] * over_sampling, ignore_index=True)
    #         temp_pd = pd.concat([temp_pd, temp_pd2], ignore_index=True)
    #     logger.info("sample inputs since requested number with sample frac %s. Initial entry: %s; Final entry: %s", sampling, current_entries,
    #                 len(temp_pd))
    #     return temp_pd
