# -*- coding: utf-8 -*-
import os
import pandas as pd
import matplotlib.pyplot as plt
import json
from sklearn import metrics
import numpy as np
from scipy import integrate
import tensorflow as tf
from keras.utils import to_categorical
from collections import defaultdict
from base.log import logger
from itertools import product
from copy import deepcopy
import time
import math
import joblib
"""
    Since we use classification but not regression,
    So we temporarily do not have R2 & nRMSE tools 

    The definition of positive and negative classes is decided by class_id.
"""


def read_config(file_name):
    """
    Reads a JSON configuration file and recursively includes other configuration files specified in the "include" field.

    This function loads the configuration from the specified file. 
    If the configuration includes an "include" field, which can be either a single file name or a list of file names, it will recursively load these included files and merge their contents into the original configuration. 
    The merging process updates existing keys with values from the included files, and includes any new keys.

    Args:
        file_name (str): The path to the JSON configuration file to read.

    Returns:
        dict: A dictionary representing the merged configuration data.

    Example:
        Assume `main_config.json` contains:
        {
            "setting1": "value1",
            "include": ["additional_config.json"]
        }

        And `additional_config.json` contains:
        {
            "setting2": "value2"
        }

        Calling `read_config('main_config.json')` will return:
        {
            "setting1": "value1",
            "setting2": "value2"
        }
    """
    with open(file_name, 'r') as file:
        Jdata = json.load(file)
        if "include" not in Jdata:
            return Jdata
        else:
            include = Jdata["include"] if isinstance(Jdata["include"], list) else [Jdata["include"]]
            Jdata.pop("include")
            Jdata_b = deepcopy(Jdata)
            for inc in include:
                inc_conf = read_config(inc)
                for key in inc_conf:
                    if key in Jdata:
                        Jdata[key].update(inc_conf[key])
                    else:
                        Jdata[key] = inc_conf[key]
            for key in Jdata_b:
                Jdata[key].update(Jdata_b[key])
    return Jdata


def calculate_normalization_params(data: pd.DataFrame, features: list, normalization_type: str, store_dir: str) -> dict:
    """
    calculate normalization params and save in store_dir
    """
    os.makedirs(store_dir, exist_ok=True)
    norm_params = {}
    
    if normalization_type == 'standard':
        means = data[features].mean()
        stds = data[features].std()
        stds.replace(0, 1, inplace=True)
        norm_params = {'means': means, 'stds': stds}
    elif normalization_type == 'robust':
        medians = data[features].median()
        q75 = data[features].quantile(0.75)
        q25 = data[features].quantile(0.25)
        iqr = q75 - q25
        iqr.replace(0, 1, inplace=True)
        norm_params = {'medians': medians, 'iqr': iqr}
    
    param_path = os.path.join(store_dir, 'norm_params')
    joblib.dump(norm_params, param_path)
    logger.info(f"Normalization parameters saved to: {param_path}")
    return norm_params

def apply_normalization(data: pd.DataFrame, features: list, norm_params: dict, normalization_type: str) -> pd.DataFrame:
    if normalization_type == 'standard':
        data[features] = (data[features] - norm_params['means']) / norm_params['stds']
    elif normalization_type == 'robust':
        data[features] = (data[features] - norm_params['medians']) / norm_params['iqr']
    return data

def handle_normalization(
    datasets: list,
    features: list,
    normalization_type: str,
    store_dir: str
) -> list:
    if normalization_type not in ['standard', 'robust']:
        return datasets 
    
    param_path = os.path.join(store_dir, 'norm_params')
    
    if os.path.exists(param_path):
        norm_params = joblib.load(param_path)
        logger.info(f"Loaded normalization params from {param_path}")
    else:
        norm_params = calculate_normalization_params(datasets[0], features, normalization_type, store_dir)

    normalized_datasets = [
        apply_normalization(dataset, features, norm_params, normalization_type)
        for dataset in datasets
    ]
    return normalized_datasets

def find_ckpt(store_dir):
    try:
        models = os.listdir(store_dir + "/ckpt")
    except:
        logger.critical("Failed to find 'ckpt' directory in store directory!! Use the final model!!")
        return store_dir
    models = list(filter(lambda s: ".keras" in s, models))
    model_path = store_dir
    i = 0
    for model in models:
        x1 = model.find(".e")
        x2 = model.find("-")
        num = float(model[x1 + 2:x2])
        if num > i:
            i = num
            model_path = os.path.join(store_dir, "ckpt", model)
    return model_path


def holdon(pop, concurrency, wait=10):
    """
    Holds the execution until the number of running processes is less than the specified concurrency level.
    
    Args:
        pop (list): A list of subprocess.Popen objects representing the running processes.
        concurrency (int): The maximum number of concurrent processes allowed.
        wait (int, optional): The number of seconds to wait before checking the process status again. Defaults to 10.
    """
    while True:
        poll = sum([1 if p.poll() is None else 0 for p in pop])  # 1(None) for running, 0(0,1) for finished
        if poll < concurrency:
            break
        time.sleep(wait)


def check_bad_data(dataframe):
    """
    Checks a pd.DataFrame for any infinite values in its features.
    """
    dataframe = dataframe.select_dtypes(include=[np.number])
    bad_feature = dataframe.columns.to_series()[np.isinf(dataframe).any()]
    if len(bad_feature) > 0:
        for bf in bad_feature:
            c = np.isinf(dataframe[bf]).values.sum()
            logger.critical("It contains " + str(c) + " inf values.")
        raise RuntimeError("Bad features:" + str(list(bad_feature)))
    else:
        logger.debug("Passing data check. No infinite feature value.")


def SaveLossPlot(task_name, training_history):
    """
    Saves plots of various training and validation metrics.
    
    Args:
        task_name (str): The base name for the saved plot files.
        training_history (keras.callbacks.History): The training history object containing metrics data.
    
    Each plot is saved as a PNG file with a filename based on the task_name and the metric.
    """
    # print("history keys: ", training_history.history.keys())
    for key in training_history.history.keys():
        if key.startswith("val"):
            continue
        plt.plot(training_history.history[key], label=f'training {key}')
        if f'val_{key}' in training_history.history.keys():
            plt.plot(training_history.history[f'val_{key}'], label=f'valid {key}')
        plt.yscale('log')
        plt.title(f'model {key}')
        plt.ylabel(key)
        plt.xlabel('epoch')
        plt.legend(loc='upper left')
        plt.savefig(f"{task_name}_{key}.png", dpi=300)
        plt.clf()

def SavePlot(func, task_name, model, total_dataset, text, class_id, **kwargs):
    for ind, dataset in enumerate(total_dataset):
        func(task_name, dataset, model, text[ind], ind == (len(total_dataset) - 1), class_id, **kwargs)

def SaveSigScorePlot(task_name, test_dataset, model, text, save, class_id, ylog=False):
    test_dataset = test_dataset[test_dataset['label'] == class_id]
    SaveScorePlot(os.path.join(task_name, "sig_"), test_dataset, model, text, save, class_id, ylog)
def SaveBkgScorePlot(task_name, test_dataset, model, text, save, class_id, ylog=False):
    test_dataset = test_dataset[test_dataset['label'] != class_id]
    SaveScorePlot(os.path.join(task_name, "bkg_"), test_dataset, model, text, save, class_id, ylog)
def SaveAllScorePlot(task_name, model, total_dataset, text, class_id, ylog=False):
    for i in range(model.label_num):
        for ind, dataset in enumerate(total_dataset):
            dataset = dataset[dataset['label'] == i]
            save = ind == (len(total_dataset) - 1)
            SaveScorePlot(os.path.join(task_name, f"proc{i}_"), dataset, model, text[ind], save, class_id, ylog)
        
def SaveScorePlot(task_name, dataset, model, text='', save=True, class_id=None, ylog=False):
    weight = dataset[model.weight_feature]
    weight = weight / weight.sum()
    
    score = pd.DataFrame(model.predict(dataset))
    if model.label_num > 2:
        if class_id is None: raise Exception("no signal class id")
        score = score.iloc[:,class_id]
    
    bins = np.linspace(0, 1, 21)
    counts, bin_edges = np.histogram(score, bins=bins, weights=weight)
    sum_weights_squared, _ = np.histogram(score, bins=bins, weights=weight**2)
    uncertainties = np.sqrt(sum_weights_squared)
    bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2
    plt.errorbar(bin_centers, counts, yerr=uncertainties, fmt='o', label=f'{text} score', capsize=3)
        
    if ylog:
        task_name += "_ylog"
        plt.yscale('log')
    plt.xlabel('Signal Score')
    plt.ylabel('Events')
    plt.legend()
    if save:
        plt.savefig(task_name + "_score.png", dpi=300)
        plt.clf()

def SaveROCPlot(task_name, test_dataset, model, text='', save=True, class_id=None):
    class_num = model.label_num
    predict = model.predict(test_dataset)
    if class_num > 2:
        label = to_categorical(test_dataset['label'], class_num)
    else:
        label = test_dataset['label']
    weight = test_dataset[model.weight_feature]

    fpr = dict()
    tpr = dict()
    auc_value = dict()
    if class_num > 2:
        for i in range(class_num):
            fpr[i], tpr[i], _ = metrics.roc_curve(label[:, i], predict[:, i], sample_weight=weight)
            sorted_index = np.argsort(fpr[i])
            fpr_list_sorted = np.array(fpr[i])[sorted_index]
            tpr_list_sorted = np.array(tpr[i])[sorted_index]
            auc_value[i] = integrate.trapezoid(y=tpr_list_sorted, x=fpr_list_sorted)
    else:
        fpr, tpr, _ = metrics.roc_curve(label, predict, sample_weight=weight)
        sorted_index = np.argsort(fpr)
        fpr_list_sorted = np.array(fpr)[sorted_index]
        tpr_list_sorted = np.array(tpr)[sorted_index]
        auc_value = integrate.trapezoid(y=tpr_list_sorted, x=fpr_list_sorted)

    plt.plot([0, 1], [0, 1], 'k--')
    if class_num > 2:
        if class_id is not None:
            plt.plot(fpr[class_id], tpr[class_id], label=f'{text} class {class_id} (AUC = {auc_value[class_id]:0.2f})')
        else:
            for i in range(class_num):
                plt.plot(fpr[i], tpr[i], label=f'{text} class {i} (AUC = {auc_value[i]:0.2f})')
    else:
        plt.plot(fpr, tpr, label=f'{text} class 1 (AUC = {auc_value:0.2f})')

    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='lower right')
    if save:
        plt.savefig(os.path.join(task_name, "_ROC.png"), dpi=300)
        plt.clf()


def SavePRPlot(task_name, test_dataset, model, text='', save=True, class_id=None):

    def filter_points(pr, rec):
        mask = (rec > 0) & (rec < 1) & (pr > 0) & (pr < 1)
        return pr[mask], rec[mask]

    class_num = model.label_num
    predict = model.predict(test_dataset)
    if class_num > 2:
        label = to_categorical(test_dataset['label'], class_num)
    else:
        label = test_dataset['label']
    weight = test_dataset[model.weight_feature]

    pr = dict()
    rec = dict()
    auc_value = dict()
    if class_num > 2:
        for i in range(class_num):
            pr[i], rec[i], _ = metrics.precision_recall_curve(label[:, i], predict[:, i], sample_weight=weight)
            pr[i], rec[i] = filter_points(pr[i], rec[i])
            sorted_index = np.argsort(pr[i])
            pr_list_sorted = np.array(pr[i])[sorted_index]
            rec_list_sorted = np.array(rec[i])[sorted_index]
            auc_value[i] = integrate.trapezoid(y=rec_list_sorted, x=pr_list_sorted)
    else:
        pr, rec, _ = metrics.precision_recall_curve(label, predict, sample_weight=weight)
        pr, rec = filter_points(pr, rec)
        sorted_index = np.argsort(pr)
        pr_list_sorted = np.array(pr)[sorted_index]
        rec_list_sorted = np.array(rec)[sorted_index]
        auc_value = integrate.trapezoid(y=rec_list_sorted, x=pr_list_sorted)

    if class_num > 2:
        if class_id is not None:
            plt.plot(pr[class_id], rec[class_id], label=f'{text} PR curve of class {class_id} (AUC = {auc_value[class_id]:0.4f})')
        else:
            for i in range(class_num):
                plt.plot(pr[i], rec[i], label=f'{text} PR curve of class {i} (AUC = {auc_value[i]:0.4f})')
    else:
        plt.plot(pr, rec, label=f'{text} PR curve of class 1 (AUC = {auc_value:0.2f})')
    # plt.yscale("log")
    plt.ylabel('Recall [ Tp/(Tp+Fn) ]')
    plt.xlabel('Precision [ Tp/(Tp+Fp) ]')
    plt.title('PR curve')
    plt.legend(loc='upper right')
    if save:
        plt.savefig(os.path.join(task_name, "_PR.png"), dpi=300)
        plt.clf()


def fbetas(pr, rec, beta):

    def fbeta(pr, rec, beta=1):
        # return max fbeta score
        npr = np.array(pr)
        nrec = np.array(rec)
        fb = (1 + beta * beta) * npr * nrec / (beta * beta * npr + nrec)
        return fb[np.isfinite(fb)].max()

    # return fbeta score over beta
    fs = []
    for b in beta:
        fs.append(fbeta(pr, rec, b))
    return fs


def SaveFBPlot(task_name, test_dataset, model, text='', save=True, class_id=None):
    class_num = model.label_num
    predict = model.predict(test_dataset)
    if class_num > 2:
        label = to_categorical(test_dataset['label'], class_num)
    else:
        label = test_dataset['label']
    weight = test_dataset[model.weight_feature]

    pr = dict()
    rec = dict()
    beta = np.array([0.5, 1, 2])
    fbeta = dict()
    if class_num > 2:
        for i in range(class_num):
            pr[i], rec[i], _ = metrics.precision_recall_curve(label[:, i], predict[:, i], sample_weight=weight)
            sorted_index = np.argsort(pr[i])
            pr_list_sorted = np.array(pr[i])[sorted_index]
            rec_list_sorted = np.array(rec[i])[sorted_index]
            fbeta[i] = fbetas(pr[i], rec[i], beta)
    else:
        pr, rec, _ = metrics.precision_recall_curve(label, predict, sample_weight=weight)
        sorted_index = np.argsort(pr)
        pr_list_sorted = np.array(pr)[sorted_index]
        rec_list_sorted = np.array(rec)[sorted_index]
        fbeta = fbetas(pr, rec, beta)

    if class_num > 2:
        if class_id is not None:
            plt.plot(beta, fbeta[class_id], label=f'{text} F score of class {class_id}')
        else:
            for i in range(class_num):
                plt.plot(beta, fbeta[i], label=f'{text} F1 score of class {i}')
    else:
        plt.plot(beta, fbeta, label=f'{text} F1 score of class 1')

    plt.xlabel('beta')
    plt.ylabel('Fbeta score')
    plt.title('Fbeta = ((1+b^2)*P*R)/(b^2*P+R)')
    plt.legend(loc='upper right')
    if save:
        plt.savefig(task_name + "_FBeta.png", dpi=300)
        plt.clf()


def evaluate_shap(pr_model, train_dataset, test_dataset, n_train, n_test, masker, class_id, workdir, max_display):
    if masker not in ["Independent", "Partition", "Impute"]:
        raise RuntimeError(f'Unavailable masker type: {masker}, please use one of "Independent", "Partition" and "Impute".')

    import shap
    np.random.seed(pr_model.random_seed)

    def balanced_sample(inputds, num):
        num_sig = num // 2
        num_bkg = num - num_sig
        
        signal_df = inputds[inputds['label'] == class_id]
        sampled_signal = signal_df.sample(n=num_sig, random_state=pr_model.random_seed)
        background_df = inputds[inputds['label'] != class_id]
        sampled_background = background_df.sample(n=num_bkg, random_state=pr_model.random_seed)
        
        output_dataframe = pd.concat([sampled_signal,sampled_background])
        return output_dataframe

    def shap_fig(type, shap_values, tag="", **kwargs):
        getattr(shap.plots, type)(shap_values, **kwargs)
        fig_size = plt.gcf().get_size_inches()  # Retrieve the figure size
        fig_size[0] *= 1.4
        plt.gcf().set_size_inches(fig_size)
        plt.savefig(f"{workdir}/shap_{type}{tag}_plot.png")
        plt.savefig(f"{workdir}/shap_{type}{tag}_plot.pdf")
        plt.close()

    train_dataset_balanced = balanced_sample(train_dataset, n_train)
    test_dataset_balanced = balanced_sample(test_dataset, n_test)

    trainx, _, __ = pr_model.prepare_data_input(train_dataset_balanced)
    testx, testy, __ = pr_model.prepare_data_input(test_dataset_balanced)

    masker = getattr(shap.maskers, masker)(data=trainx)
    explainer = shap.Explainer(
        pr_model.model.predict, masker=masker, feature_names=[f.replace('fb_', '') for f in pr_model.nominal_features])  # TODO: check sequence
    shap_values = explainer(testx)

    # local shap plots
    # shap_fig("waterfall",shap_values[0,:,0])
    # shap_fig("scatter",shap_values[:,"fb_mtx_jet",0],"fb_mtx_jet")
    try:
        signal_shap = shap_values[..., class_id]
        clabels = ["Signal" if testy[i][class_id] == 1 else "Bkg" for i in range(testy.shape[0])]
    except:
        signal_shap = shap_values
        clabels = ["Signal" if testy[i] else "Bkg" for i in range(testy.shape[0])]

    shap_values_df = pd.DataFrame(signal_shap.values, columns=signal_shap.feature_names)
    mean_abs_shap_values = shap_values_df.abs().mean().sort_values(ascending=False)
    mean_abs_shap_values.to_csv(f'{workdir}/mean_abs_shap_values.csv')

    shap_fig("heatmap", signal_shap, max_display=max_display)
    shap_fig("heatmap", signal_shap, "feat", max_display=max_display, feature_values=signal_shap.abs.max(0))

    shap_fig("violin", signal_shap, max_display=max_display)

    shap_fig("bar", signal_shap, max_display=max_display)
    shap_fig("bar", signal_shap, "corr0", max_display=max_display, clustering_cutoff=0)
    shap_fig("bar", signal_shap, "corr1", max_display=max_display, clustering_cutoff=1)
    shap_fig("bar", signal_shap, "corr25", max_display=max_display, clustering_cutoff=0.25)
    shap_fig("bar", signal_shap, "corr75", max_display=max_display, clustering_cutoff=0.75)
    
    shap_fig("bar", signal_shap.cohorts(clabels), "cohorts", max_display=max_display)
    shap_fig("bar", signal_shap.cohorts(clabels), "cohorts_corr0", max_display=max_display, clustering_cutoff=0)
    shap_fig("bar", signal_shap.cohorts(clabels), "cohorts_corr1", max_display=max_display, clustering_cutoff=1)
    shap_fig("bar", signal_shap.cohorts(clabels), "cohorts_corr25", max_display=max_display, clustering_cutoff=0.25)
    shap_fig(
        "bar", signal_shap.cohorts(clabels), "cohorts_corr75", max_display=len(pr_model.nominal_features),
        clustering_cutoff=0.75)  # possible index range issue, let max_display=len(pr_model.nominal_features) to solve it

    shap_fig("bar", signal_shap.mean(0), "mean", max_display=max_display)
    shap_fig("bar", signal_shap.mean(0), "mean_corr0", max_display=max_display, clustering_cutoff=0)
    shap_fig("bar", signal_shap.mean(0), "mean_corr1", max_display=max_display, clustering_cutoff=1)
    shap_fig("bar", signal_shap.mean(0), "mean_corrp25", max_display=max_display, clustering_cutoff=0.25)
    shap_fig("bar", signal_shap.mean(0), "mean_corrp75", max_display=max_display, clustering_cutoff=0.75)
    
    shap_fig("bar", signal_shap.cohorts(clabels).mean(0), "mean_cohorts", max_display=max_display)
    shap_fig("bar", signal_shap.cohorts(clabels).mean(0), "mean_cohorts_corr0", max_display=max_display, clustering_cutoff=0)
    shap_fig("bar", signal_shap.cohorts(clabels).mean(0), "mean_cohorts_corr1", max_display=max_display, clustering_cutoff=1)
    shap_fig("bar", signal_shap.cohorts(clabels).mean(0), "mean_cohorts_corr25", max_display=max_display, clustering_cutoff=0.25)
    shap_fig("bar", signal_shap.cohorts(clabels).mean(0), "mean_cohorts_corr75", max_display=len(pr_model.nominal_features), clustering_cutoff=0.75)

    shap_fig("beeswarm", signal_shap, max_display=max_display)
    shap_fig("beeswarm", signal_shap.abs, "abs", max_display=max_display)


def compute_AUC_PR(test_dataset, model, class_id=0):

    def filter_points(pr, rec):
        mask = (rec > 0) & (rec < 1) & (pr > 0) & (pr < 1)
        return pr[mask], rec[mask]

    class_num = model.label_num
    predict = model.predict(test_dataset)
    if class_num > 2:
        label = to_categorical(test_dataset['label'], class_num)
    else:
        label = test_dataset['label']
    weight = test_dataset[model.weight_feature]

    if class_num > 2:
        pr, rec, _ = metrics.precision_recall_curve(label[:, class_id], predict[:, class_id], sample_weight=weight)
    else:
        pr, rec, _ = metrics.precision_recall_curve(label, predict, sample_weight=weight)

    pr, rec = filter_points(pr, rec)
    sorted_index = np.argsort(pr)
    pr_list_sorted = np.array(pr)[sorted_index]
    rec_list_sorted = np.array(rec)[sorted_index]
    auc_value = integrate.trapezoid(y=rec_list_sorted, x=pr_list_sorted)

    return auc_value


def compute_Zn(test_dataset, model, relativeBkgUncert=0.2, lumi_scale=1, class_id=0):
    class_num = model.label_num
    predict = model.predict(test_dataset)
    weight = test_dataset[model.weight_feature]
    predict_score = predict[..., class_id]
    label = test_dataset['label'] == class_id
    ZnMax = 0
    thresholds = list(np.linspace(start=0.5, stop=0.9, num=40)) + list(np.linspace(start=0.9, stop=1.0, num=50))
    for threshold in thresholds:
        tn, fp, fn, tp = metrics.confusion_matrix(y_true=label, y_pred=(predict_score > threshold), sample_weight=weight).ravel()
        nb = fp * lumi_scale
        sig = tp * lumi_scale
        Err = relativeBkgUncert * nb
        E2 = Err * Err
        n = sig + nb
        try:
            dll = n * math.log(((sig + nb) * (nb + E2)) / (nb * nb + (sig + nb) * E2)) - \
                    ((nb * nb) / (E2)) * math.log((nb * nb + (sig + nb) * E2) / (nb * (nb + E2)))
            nsigma = math.sqrt(2. * dll)
        except:
            nsigma = 0
        if np.isnan(nsigma):
            continue
        if nsigma > ZnMax:
            ZnMax = nsigma
    return ZnMax


def compute_ZnBinned(test_dataset, model, relativeBkgUncert=0.2, start=0.9, num_thresholds=20, class_id=0):
    #TODO: should we use chi2.sf and norm.isf
    class_num = model.label_num
    predict = model.predict(test_dataset)
    weight = test_dataset[model.weight_feature]
    predict_score = predict[..., class_id]
    label = test_dataset['label'] == class_id

    thresholds = list(np.linspace(start=start, stop=1.0, num=num_thresholds))
    N = len(thresholds) - 1
    chi2_value = 0
    for threshold, threshold_up in zip(thresholds[:-1], thresholds[1:]):
        tn, fp, fn, tp = metrics.confusion_matrix(
            y_true=label, y_pred=np.logical_and(predict_score > threshold, predict_score < threshold_up), sample_weight=weight).ravel()
        nb = fp
        sig = tp

        Err = relativeBkgUncert * nb
        E2 = Err * Err
        n = sig + nb
        if ((nb * nb + (sig + nb) * E2) == 0) or (E2 == 0) or (nb * (nb + E2)) == 0:
            continue
        dll = n * math.log(((sig + nb) * (nb + E2)) / (nb * nb + (sig + nb) * E2)) - \
                ((nb * nb) / (E2)) * math.log((nb * nb + (sig + nb) * E2) / (nb * (nb + E2)))
        nsigma = math.sqrt(2. * dll)
        if np.isnan(nsigma):
            continue
        chi2_value += nsigma * nsigma
    pvalue = chi2.sf(chi2_value, df=N)
    significance = norm.isf(pvalue)
    return significance
