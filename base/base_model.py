# -*- coding: utf-8 -*-

import tensorflow as tf
from tensorflow import keras
from keras import layers, backend as K
from base.log import logger
from abc import abstractmethod
import pandas as pd
import datetime
from sklearn.preprocessing import OneHotEncoder
import numpy as np
from itertools import permutations
from base.callbacks import PerformanceVisualizationCallback
import os
from base.scheduler import *
from base.metrics import *
from base.log import logger
from optuna.integration import TFKerasPruningCallback


class BaseModel:
    """
    BaseModel is an abstract base class for NN models.
    
    Attributes:
        train_config (dict): Configuration parameters for training the model.
        model (keras.Model or None): The machine learning model instance.
        
        random_seed (int): Random seed for reproducibility.
        label_group (list): List of label names.
        label_num (int): Number of labels.
        nominal_features (list): List of nominal feature names.
        weight_feature (str): Feature used for weighting.
        class_weight (dict): Weights for each class.
        class_id (int): id of signal class.
        
        optimizer (str): Optimizer for training the model.
        optim_params (dict): Parameters for the optimizer.
        batch_size (int): Batch size for training.
        epochs (int): Number of training epochs.
        dropout (float or None): Dropout rate for regularization (if applicable).
        reg_lambda (float): Regularization parameter.
        activation (str): Activation function.
        earlystop (bool or None): Early stopping flag (if applicable).
        ckpt (str or None): Checkpoint path for saving model (if applicable).
        reducelr (bool or None): Flag for reducing learning rate on plateau (if applicable).
        epoch_pv (bool): Flag for using epoch-wise particle values.
        lr_scheduler (object or None): Learning rate scheduler (if applicable).
    """

    def __init__(self, sample_config, train_config=None):
        """
        Initializes the BaseModel with sample and training configurations.

        Args:
            sample_config (dict): Configuration for the dataset sample.
            train_config (dict, optional): Configuration for training the model. If not provided, default values are used.
        Notes:
            Mandatory items of sample_config: label_group, nominal_features, weight_branch, 
            Optional items of sample_config: random_seed, class_weight, class_id
            Optional items of train_config: optimizer, optim_params, loss, loss_params, batch_size, epochs, 
                                            dropout, reg_lambda, activation, earlystop, ckpt, reducelr, epoch_pv, lr_scheduler, lrsc_params
        """
        train_config = {} if train_config is None else train_config
        self.train_config = train_config
        self.model = None

        self.random_seed = sample_config.get('random_seed', 12345)
        self.label_group = sample_config['label_group']
        self.label_num = len(self.label_group)
        self.nominal_features = sample_config['nominal_features']
        self.weight_feature = sample_config['weight_branch']

        if 'class_weight' in sample_config and len(sample_config['class_weight']) != self.label_num:
            raise RuntimeError('The length of label_group and class_weight are not consistent.')
        self.class_weight = {ind: 1. for ind in range(self.label_num)} if 'class_weight' not in sample_config else  \
                            {ind: float(wei) for ind, wei in enumerate(sample_config['class_weight'])}
        self.class_id = sample_config.get("class_id", 0)

        bkg_sampling_factor = min(sample_config.get('sampling', {'': 1}).values())  # assume same factors for all bkgs
        sig_sampling_factor = max(sample_config.get('sampling', {'': 1}).values())
        self.sampling_factor = bkg_sampling_factor / sig_sampling_factor

        self.optimizer = "AdamW" if 'optimizer' not in train_config else train_config['optimizer']
        self.optim_params = {"learning_rate": 0.0001} if 'optim_params' not in train_config else train_config['optim_params']
        self.loss = 'Crossentropy' if 'loss' not in train_config else train_config['loss']
        self.loss_params = {} if 'loss_params' not in train_config else train_config['loss_params']
        self.batch_size = 256 if 'batch_size' not in train_config else train_config['batch_size']
        self.epochs = 200 if 'epochs' not in train_config else train_config['epochs']
        self.dropout = None if 'dropout' not in train_config else train_config['dropout']
        self.reg_lambda = 0 if 'reg_lambda' not in train_config else train_config['reg_lambda']
        self.activation = 'relu' if 'activation' not in train_config else train_config['activation']
        self.earlystop = None if 'earlystop' not in train_config else train_config['earlystop']
        self.ckpt = None if 'ckpt' not in train_config else train_config['ckpt']
        self.reducelr = None if 'reducelr' not in train_config else train_config['reducelr']
        self.epoch_pv = False if 'epoch_pv' not in train_config else train_config['epoch_pv']
        self.lr_scheduler = None if 'lr_scheduler' not in train_config else train_config['lr_scheduler']
        self.lrsc_params = {} if 'lrsc_params' not in train_config else train_config['lrsc_params']

    def get_model(self):
        """ Returns: self.model """
        return self.model

    def load_model(self, model_path, **kwargs):
        """ 
        Loads a model saved via model.save().

        Returns: keras.models.load_model
        """
        if model_path.endswith(".keras"):
            self.model = keras.models.load_model(model_path, **kwargs)
        else:
            self.model = keras.models.load_model(model_path + '/model.keras', **kwargs)

    @abstractmethod
    def build_model(self, nominal_input):
        """ 
        Abstract method to be implemented to create the model. More details in https://keras.io/api/models/model/
        """
        pass

    def prepare_data_input(self, data):
        """
        Prepares the input data for model training.
        This method prepares the features, target and weight arrays for the model. 
        The class_weight can't be set in model.fit and therefore is implemented here.

        Args:
            data (pd.DataFrame): The input data containing features, labels and weight.

        Returns:
            tuple: A tuple containing:
                - inputs (dataframe or list of DataFrame): A list with features DataFrame or one DataFrame
                - target (ndarray): An array of target labels, either one-hot encoded if `label_num` > 2, or just 0/1 otherwise.
                - weight (ndarray): An array of weights corresponding to each event.
        """

        nominal_input = self.get_nominal_feature_embedding(data)
        if self.label_num > 2:
            target = keras.utils.to_categorical(data['label'], self.label_num)
        else:
            target = data['label'].to_numpy()
        weight = data[self.weight_feature].to_numpy()
        scaled_weights = np.array([w * self.class_weight.get(l, 1) for w, l in zip(list(weight), list(data['label']))])
        return nominal_input, target, scaled_weights

    def prepare_model_input(self):
        """ constructs the input layers for the model """
        nominal_input = layers.Input(shape=(len(self.nominal_features),), name='nominal_features', dtype='float32')
        return nominal_input

    def fit(self, train_dataset, val_dataset, storedir, trial=None):
        """
        Trains the model using the provided datasets and saves the training logs and checkpoints.

        Args:
            train_dataset (pd.DataFrame): A pd.DataFrame containing the training data (training_features, training_labels, training_weights). 
            val_dataset (tuple or None): A tuple containing the validation data (validation_features, validation_labels, validation_weights).
                                        If validation data is not available, this should be `None`.
            storedir (str): The directory where the logs and model checkpoints will be saved. This should be a valid directory path.

        Returns:
            keras.callbacks.History: A History object that contains the training loss and metrics values recorded during training.
        """
        train_x, train_y, train_weight = self.prepare_data_input(train_dataset)

        inputs = self.prepare_model_input()
        model_outputs = self.build_model(inputs)
        if self.label_num > 2:
            outputs = layers.Dense(units=self.label_num, activation='softmax')(model_outputs)
        else:
            outputs = layers.Dense(units=1, activation='sigmoid')(model_outputs)
        self.model = keras.Model(inputs=inputs, outputs=outputs, name='model')
        self.model.summary(print_fn=logger.info)

        # optimizer = getattr(keras.optimizers.experimental, self.optimizer)(**self.optim_params)
        optimizer = getattr(keras.optimizers, self.optimizer)(**self.optim_params)

        # https://keras.io/api/metrics/
        # precision = TP/(TP+FP) = S/(S+B) in SR, can be influenced by class weight!
        # TPR = recall = sensitivity = TP/(TP+FN), signal acceptance rate
        # specificity = TN/(TN+FP) = 1-FPR, background rejection rate
        # ROC: TPR v.s. FPR, aka sensitivity v.s. 1-specificity
        # class_id = self.class_id (0: 1st class).
        safe_class_id = self.class_id if self.label_num > 2 else None
        evaluate_metrics = [
            # 'accuracy',
            # keras.metrics.SensitivityAtSpecificity(specificity=0.999, class_id=safe_class_id, name="signalAccepAt999"),
            # keras.metrics.SensitivityAtSpecificity(specificity=0.9995, class_id=safe_class_id, name="signalAccepAt9995"),
            keras.metrics.SpecificityAtSensitivity(sensitivity=0.10, class_id=safe_class_id, name="bkgRejectAt10"),
            keras.metrics.SpecificityAtSensitivity(sensitivity=0.05, class_id=safe_class_id, name="bkgRejectAt5"),
            RealAUC(class_id=self.class_id,name="PR_AUC_train"),
        ]
        # if label_num > 2, use category loss, else use binary
        if self.loss == 'Focal':
            if self.label_num > 2:
                cce = keras.losses.CategoricalFocalCrossentropy(reduction="mean_with_sample_weight",**self.loss_params)
                self.model.compile(optimizer=optimizer, loss=cce, weighted_metrics=evaluate_metrics)
            else:
                bce = keras.losses.BinaryFocalCrossentropy(reduction="mean_with_sample_weight",**self.loss_params)
                self.model.compile(optimizer=optimizer, loss=bce, weighted_metrics=evaluate_metrics)
        else:
            if self.label_num > 2:
                cce = keras.losses.CategoricalCrossentropy(reduction="mean_with_sample_weight")
                self.model.compile(optimizer=optimizer, loss=cce, weighted_metrics=evaluate_metrics)
            else:
                bce = keras.losses.BinaryCrossentropy(reduction="mean_with_sample_weight")
                self.model.compile(optimizer=optimizer, loss=bce, weighted_metrics=evaluate_metrics)

        log_dir = os.path.join(storedir, "tblog")
        file_writer = tf.summary.create_file_writer(os.path.join(log_dir, "metrics"))
        file_writer.set_as_default()
        callback_list = []
        tensorboard_callback = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        callback_list.append(tensorboard_callback)

        if self.earlystop is not None:
            if (val_dataset is None) and self.earlystop['monitor'].startswith("val_"):
                self.earlystop['monitor'] = self.earlystop['monitor'][4:]
            earlystop_callback = keras.callbacks.EarlyStopping(**self.earlystop)
            callback_list.append(earlystop_callback)

        if self.ckpt is not None:
            if not isinstance(self.ckpt, list):
                self.ckpt = [self.ckpt]
            for ckpt_config in self.ckpt:
                if (val_dataset is None) and ckpt_config['monitor'].startswith("val_"):
                    ckpt_config['monitor'] = ckpt_config['monitor'][4:]
                if 'loss' in ckpt_config['monitor']:
                    name = 'l'
                elif 'pr_auc' in ckpt_config['monitor']:
                    name = 'pr'
                fileformat = f"/ckpt/weights.e{{epoch:02d}}-{name}{{{ckpt_config['monitor']}:.3f}}.keras"
                modelcheckpoint_callback = keras.callbacks.ModelCheckpoint(filepath=storedir + fileformat, save_best_only=True, **ckpt_config)
                callback_list.append(modelcheckpoint_callback)

        if self.lr_scheduler is not None:
            # "polynomial_decay", "time_based_decay", "exp_decay", "step_based_decay", "square_root_decay"
            # or "warmup_" + a kind of decay
            kwargs_dict = self.lrsc_params
            kwargs_dict["initial_learning_rate"] = self.optim_params["learning_rate"]
            if "polynomial_decay" in self.lr_scheduler or "time_based_decay" in self.lr_scheduler:
                kwargs_dict["epochs"] = self.epochs
            if "warmup" in self.lr_scheduler:
                kwargs_dict["decay_scheduler"] = self.lr_scheduler.split("_", 1)[1]
                Scheduler = WarmupScheduler(**kwargs_dict)
            else:
                Scheduler = decay_dict[self.lr_scheduler](**kwargs_dict)

            lr_scheduler_callback = keras.callbacks.LearningRateScheduler(Scheduler)
            callback_list.append(lr_scheduler_callback)

        if self.reducelr is not None:
            if (val_dataset is None) and self.reducelr['monitor'].startswith("val_"):
                self.reducelr['monitor'] = self.reducelr['monitor'][4:]
            reduce_lr = keras.callbacks.ReduceLROnPlateau(**self.reducelr)
            callback_list.append(reduce_lr)

        if self.epoch_pv and (val_dataset is not None):
            # very slow
            performance_cbk = PerformanceVisualizationCallback(
                model=self.model, validation_data=self.prepare_data_input(val_dataset), image_dir=storedir + "/performance_vizualizations")
            callback_list.append(performance_cbk)

        logger.info("Set log folder: %s", log_dir)
        logger.info("Set model ckpt folder: %s", storedir + "/ckpt")

        params = {
            'x': train_x,
            'y': train_y,
            'batch_size': self.batch_size,
            'epochs': self.epochs,
            # 'class_weight': self.class_weight, # ValueError: You cannot `class_weight` and `sample_weight` at the same time.
            'sample_weight': train_weight,
            'callbacks': callback_list
        }
        if val_dataset is not None:
            val_data = self.prepare_data_input(val_dataset)
            params['validation_data'] = val_data
        training_history = self.model.fit(**params)

        try:
            keras.utils.plot_model(self.model, show_shapes=True, to_file=storedir + "/model.png")
        except:
            pass
        return training_history

    def save(self, store_dir):
        """ Save a model via model.save() """
        self.model.save(store_dir + "/model.keras")

    def predict(self, dataset):
        """
        https://www.tensorflow.org/api_docs/python/tf/keras/Model#predict
        Generates output predictions for the input samples.
        Computation is done in batches. This method is designed for batch processing of large numbers of inputs. 

        Args:
            dataset (pd.DataFrame): containing features, label and event weight

        Returns:
            NumPy array(s) of predictions.
        """
        x, y, w = self.prepare_data_input(dataset)
        return self.model.predict(x, batch_size=self.batch_size)
        #return self.model.predict_on_batch(x)

    def evaluate(self, dataset):
        """
        https://www.tensorflow.org/api_docs/python/tf/keras/Model#evaluate
        Returns the loss value & metrics values for the model in test mode. Computation is done in batches.

        Args:
            dataset (pd.DataFrame): containing features, label and event weight

        Returns:
            Scalar test loss (if the model has a single output and no metrics) or list of scalars (if the model has multiple outputs and/or metrics). 
            The attribute model.metrics_names will give you the display labels for the scalar outputs.
        """
        x, y, w = self.prepare_data_input(dataset)
        return self.model.evaluate(x, y, sample_weight=w, batch_size=self.batch_size, return_dict=True)

    def get_nominal_feature_embedding(self, data):
        """
        Prepares DataFrame containing the nominal features

        Args:
            data (pd.DataFrame): The input data containing features, labels and weight.

        Returns:
            pd.DataFrame: The input data containing features.
        """
        nom_list = []
        for f in self.nominal_features:
            nom_list.append(data[f])
        out_n_features = pd.concat(nom_list, axis=1).to_numpy()
        return out_n_features
