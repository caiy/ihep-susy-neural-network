# -*- coding: utf-8 -*-
import os
import pandas as pd
import json
import uproot
import tensorflow as tf
import numpy as np
from base import base_model, file_handler


class PredictModel(base_model.BaseModel):

    def __init__(self, sample_config):
        """ We don't need to train the model so here we just init with sample_config. """
        super().__init__(sample_config, None)

    def build_model(self, nominal_input, particle_input):
        """ We don't need to build model so here we just return None """
        return None

    def get_prediction(self, data, output_path):
        """
        Generates prediction scores for input data and saves the results in ROOT files.

        Args:
        data(pd.DataFrame): The input data containing features for prediction, labels, weights, and a 'TreeName' column.
        output_path (str): The path where the output ROOT files will be saved.
        """
        inputx, _, __ = self.prepare_data_input(data)  # class_weight is applied in prepare_data_input
        tree = set(data['TreeName'])
        dsid_array = data[['fb_mcChannelNumber','TreeName',self.weight_feature]]
        pred_scores = self.model.predict(inputx)
        cols1 = [f"class{i}_score" for i in range(0, pred_scores.shape[1])]
        
        output = pd.concat([data.reset_index(drop=True), pd.DataFrame(pred_scores, columns=cols1, dtype=np.double)], axis=1)
        small_output = pd.concat([dsid_array.reset_index(drop=True), pd.DataFrame(pred_scores, columns=cols1, dtype=np.double)], axis=1)
        os.makedirs(output_path, exist_ok=True)
        for tree_name in tree:
            name = tree_name.replace("_Nom", "")
            ifile = uproot.recreate(output_path + "/" + name + ".root")
            idata = output.loc[output['TreeName'] == tree_name]
            # idata = idata.drop('TreeName', axis=1)
            ifile["myTree"] = idata
            ifile.close()
            
            sfile = uproot.recreate(output_path + "/" + name + "_score.root")
            score = small_output.loc[small_output['TreeName'] == tree_name]
            # score = score.drop('TreeName', axis=1)
            sfile["scoreTree"] = score
            sfile.close()