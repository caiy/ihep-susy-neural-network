import tensorflow as tf
import numpy as np
from scipy import integrate
"""
The definition of positive and negative classes is decided by class_id
"""


class RealAUC(tf.keras.metrics.Metric):
    """
    precision = tp / (tp+fp), and is influenced by class_weight.
    In this class, tp/class_weight is used to get the real precision.
    has been valitaed with keras.metrics.Precision, the values are the same when class_weight=1.
    validation block:
    evaluate_metrics += [
            RealAUC(num_thresholds=10, class_id=0, class_weight=1, name="pr_auc_i"),
            keras.metrics.AUC(num_thresholds=10,curve='PR', name='pr_auc_k', multi_label=True, label_weights=[1]+[0]*(self.label_num-1) ),
    ]   
    """

    def __init__(self, name='real_AUC', num_thresholds=200, class_id=0, class_weight=1, **kwargs):
        super(RealAUC, self).__init__(name=name, **kwargs)
        self.num_thresholds = num_thresholds
        self.true_positives = self.add_weight(name='tp', shape=(num_thresholds,), initializer='zeros')
        self.false_positives = self.add_weight(name='fp', shape=(num_thresholds,), initializer='zeros')
        self.true_negatives = self.add_weight(name='tn', shape=(num_thresholds,), initializer='zeros')
        self.false_negatives = self.add_weight(name='fn', shape=(num_thresholds,), initializer='zeros')
        self.class_id = class_id
        self.class_weight = class_weight
        self.thresholds = np.linspace(start=0.0, stop=1.0, num=num_thresholds)

    def update_state(self, y_true, y_pred, sample_weight=None):
        try:
            y_true_bools = tf.cast(y_true[:, self.class_id], tf.bool)
            class_scores = y_pred[:, self.class_id]
        except:
            y_true_bools = tf.cast(y_true, tf.bool)
            class_scores = tf.cast(y_pred, tf.float32)

        true_positives_sum_list = []
        false_positives_sum_list = []
        true_negatives_sum_list = []
        false_negatives_sum_list = []
        for i, threshold in enumerate(self.thresholds):
            binary_predictions = tf.greater_equal(class_scores, threshold)
            true_positives = tf.cast(tf.logical_and(binary_predictions, y_true_bools), tf.float32)
            false_positives = tf.cast(tf.logical_and(binary_predictions, tf.logical_not(y_true_bools)), tf.float32)
            true_negatives = tf.cast(tf.logical_and(tf.logical_not(binary_predictions), tf.logical_not(y_true_bools)), tf.float32)
            false_negatives = tf.cast(tf.logical_and(tf.logical_not(binary_predictions), y_true_bools), tf.float32)
            if sample_weight is not None:
                sample_weight = tf.cast(sample_weight, tf.float32)
                true_positives = tf.multiply(true_positives, sample_weight)
                false_positives = tf.multiply(false_positives, sample_weight)
                true_negatives = tf.multiply(true_negatives, sample_weight)
                false_negatives = tf.multiply(false_negatives, sample_weight)

            true_positives_sum = tf.reduce_sum(true_positives) / self.class_weight
            false_positives_sum = tf.reduce_sum(false_positives)
            true_negatives_sum = tf.reduce_sum(true_negatives)
            false_negatives_sum = tf.reduce_sum(false_negatives) / self.class_weight

            true_positives_sum_list.append(true_positives_sum)
            false_positives_sum_list.append(false_positives_sum)
            true_negatives_sum_list.append(true_negatives_sum)
            false_negatives_sum_list.append(false_negatives_sum)

        self.true_positives.assign_add(true_positives_sum_list)
        self.false_positives.assign_add(false_positives_sum_list)
        self.true_negatives.assign_add(true_negatives_sum_list)
        self.false_negatives.assign_add(false_negatives_sum_list)

    def interpolate_pr_auc(self):
        """
        https://www.tensorflow.org/api_docs/python/tf/keras/metrics/AUC#interpolate_pr_auc
        AUC = int{precision d(recall)} = int{precision d(tp)} / total_pos_weight = slope / total_pos_weight * int{precision dP}
        int_A^B{Precision.dP} = dTP + intercept * log(P_B / P_A), where intercept = TP_A - slope * P_A = TP_B - slope * P_B
        """
        dtp = tf.subtract(self.true_positives[:-1], self.true_positives[1:])
        p = tf.add(self.true_positives, self.false_positives)
        dp = tf.subtract(p[:-1], p[1:])
        prec_slope = tf.math.divide_no_nan(dtp, tf.maximum(dp, 0))
        intercept = tf.subtract(self.true_positives[1:], tf.multiply(prec_slope, p[1:]))
        safe_p_ratio = tf.where(tf.logical_and(p[:-1] > 0, p[1:] > 0), tf.math.divide_no_nan(p[:-1], tf.maximum(p[1:], 0)), tf.ones_like(p[1:]))
        pr_auc_increment = tf.math.divide_no_nan(
            tf.multiply(
                prec_slope,
                (tf.add(dtp, tf.multiply(intercept, tf.math.log(safe_p_ratio)))),
            ), tf.maximum(tf.add(self.true_positives[1:], self.false_negatives[1:]), 0))
        return tf.reduce_sum(pr_auc_increment)

    def result(self):
        return self.interpolate_pr_auc()

    def reset_states(self):
        # The state of the metric will be reset at the start of each epoch.
        self.true_positives.assign(tf.zeros_like(self.true_positives))
        self.false_positives.assign(tf.zeros_like(self.false_positives))
        self.true_negatives.assign(tf.zeros_like(self.true_negatives))
        self.false_negatives.assign(tf.zeros_like(self.false_negatives))

    def get_config(self):
        base_config = super().get_config()
        config = {"num_thresholds": self.num_thresholds, "class_id": self.class_id, "class_weight": self.class_weight}
        return {**base_config, **config}
