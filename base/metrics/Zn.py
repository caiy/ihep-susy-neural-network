import tensorflow as tf
import numpy as np
from .metrics_utils import calZn
"""
The definition of positive and negative classes is decided by class_id
"""


class Zn(tf.keras.metrics.Metric):

    def __init__(self, name='Zn', class_id=0, class_weight=1, relativeBkgUncert=0.2, num_thresholds=60, **kwargs):
        super(Zn, self).__init__(name=name, **kwargs)

        self.true_positives = self.add_weight(name='tp', shape=(num_thresholds,), initializer='zeros')
        self.false_positives = self.add_weight(name='fp', shape=(num_thresholds,), initializer='zeros')
        self.class_id = class_id
        self.class_weight = class_weight
        self.thresholds = np.linspace(start=0.7, stop=1.0, num=num_thresholds)  # bin_width = 0.01
        self.relativeBkgUncert = relativeBkgUncert

    def update_state(self, y_true, y_pred, sample_weight=None):
        try:
            y_true_bools = tf.cast(y_true[:, self.class_id], tf.bool)
            class_scores = y_pred[:, self.class_id]
        except:
            y_true_bools = tf.cast(y_true, tf.bool)
            class_scores = tf.cast(y_pred, tf.float32)

        true_positives_sum_list = []
        false_positives_sum_list = []
        for i, threshold in enumerate(self.thresholds):
            binary_predictions = tf.greater_equal(class_scores, threshold)
            true_positives = tf.cast(tf.logical_and(binary_predictions, y_true_bools), tf.float32)
            false_positives = tf.cast(tf.logical_and(binary_predictions, tf.logical_not(y_true_bools)), tf.float32)
            if sample_weight is not None:
                sample_weight = tf.cast(sample_weight, tf.float32)
                true_positives = tf.multiply(true_positives, sample_weight)
                false_positives = tf.multiply(false_positives, sample_weight)

            true_positives_sum = tf.reduce_sum(true_positives) / self.class_weight
            false_positives_sum = tf.reduce_sum(false_positives)

            true_positives_sum_list.append(true_positives_sum)
            false_positives_sum_list.append(false_positives_sum)

        self.true_positives.assign_add(true_positives_sum_list)
        self.false_positives.assign_add(false_positives_sum_list)

    def result(self):
        return tf.reduce_max(calZn(self.true_positives, self.false_positives, self.relativeBkgUncert))

        # sig = self.true_positives
        # nb = self.false_positives
        # Err = self.relativeBkgUncert * nb
        # E2 = Err*Err
        # n = sig + nb
        # dll = n * tf.math.log(tf.math.divide_no_nan(((sig + nb) * (nb + E2)), (nb * nb + (sig + nb) * E2))) - \
        #         tf.math.divide_no_nan((nb * nb), (E2)) * tf.math.log(tf.math.divide_no_nan((nb * nb + (sig + nb) * E2), (nb * (nb + E2)) ))
        # nsigma = tf.sqrt(2. * dll + 1e-10 )
        # return tf.reduce_max(nsigma)

    def reset_states(self):
        # The state of the metric will be reset at the start of each epoch.
        self.true_positives.assign(tf.zeros_like(self.true_positives))
        self.false_positives.assign(tf.zeros_like(self.false_positives))

    def get_config(self):
        base_config = super().get_config()
        config = {
            "num_thresholds": self.num_thresholds,
            "relativeBkgUncert": self.relativeBkgUncert,
            "class_id": self.class_id,
            "class_weight": self.class_weight
        }
        return {**base_config, **config}
