import tensorflow as tf
import numpy as np
from .metrics_utils import calZn
from scipy.stats import chi2, norm
"""
The definition of positive and negative classes is decided by class_id
"""


class ZnBinned(tf.keras.metrics.Metric):

    def __init__(self, name='ZnBinned', class_id=0, class_weight=1, relativeBkgUncert=0.2, num_thresholds=30, **kwargs):
        super(ZnBinned, self).__init__(name=name, **kwargs)
        self.true_positives = self.add_weight(name='tp', shape=(num_thresholds - 1,), initializer='zeros')
        self.false_positives = self.add_weight(name='fp', shape=(num_thresholds - 1,), initializer='zeros')
        self.class_id = class_id
        self.class_weight = class_weight
        self.thresholds = np.linspace(start=0.7, stop=1.0, num=num_thresholds)  # bin_width = 0.01
        self.relativeBkgUncert = relativeBkgUncert

    def update_state(self, y_true, y_pred, sample_weight=None):
        try:
            y_true_bools = tf.cast(y_true[:, self.class_id], tf.bool)
            class_scores = y_pred[:, self.class_id]
        except:
            y_true_bools = tf.cast(y_true, tf.bool)
            class_scores = tf.cast(y_pred, tf.float32)

        true_positives_sum_list = []
        false_positives_sum_list = []
        for threshold, threshold_high in zip(self.thresholds[:-1], self.thresholds[1:]):
            binary_predictions = tf.logical_and(tf.greater_equal(class_scores, threshold), tf.less_equal(class_scores, threshold_high))
            true_positives = tf.cast(tf.logical_and(binary_predictions, y_true_bools), tf.float32)
            false_positives = tf.cast(tf.logical_and(binary_predictions, tf.logical_not(y_true_bools)), tf.float32)
            if sample_weight is not None:
                sample_weight = tf.cast(sample_weight, tf.float32)
                true_positives = tf.multiply(true_positives, sample_weight)
                false_positives = tf.multiply(false_positives, sample_weight)

            true_positives_sum = tf.reduce_sum(true_positives) / self.class_weight
            false_positives_sum = tf.reduce_sum(false_positives)

            true_positives_sum_list.append(true_positives_sum)
            false_positives_sum_list.append(false_positives_sum)

        self.true_positives.assign_add(true_positives_sum_list)
        self.false_positives.assign_add(false_positives_sum_list)

    def result(self):
        # https://root-forum.cern.ch/t/significance-bin-by-bin/37269/2
        N = len(self.true_positives)
        chi2_value = 0
        for sig, nb in zip(self.true_positives, self.false_positives):
            Zn = calZn(sig, nb, self.relativeBkgUncert)
            chi2_value += Zn * Zn

        pvalue = chi2.sf(chi2_value, df=N)
        significance = norm.isf(pvalue)
        return significance

    def reset_states(self):
        # The state of the metric will be reset at the start of each epoch.
        self.true_positives.assign(tf.zeros_like(self.true_positives))
        self.false_positives.assign(tf.zeros_like(self.false_positives))

    def get_config(self):
        base_config = super().get_config()
        config = {
            "num_thresholds": self.num_thresholds,
            "relativeBkgUncert": self.relativeBkgUncert,
            "class_id": self.class_id,
            "class_weight": self.class_weight
        }
        return {**base_config, **config}
