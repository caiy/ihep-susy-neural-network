import tensorflow as tf
import numpy as np
"""
The definition of positive and negative classes is decided by class_id
"""


class RealPrecision(tf.keras.metrics.Metric):
    """
    precision = tp / (tp+fp), and is influenced by class_weight.
    In this class, tp/class_weight is used to get the real precision.
    has been valitaed with keras.metrics.Precision, the values are the same when class_weight=1.
    validation block:
    evaluate_metrics += [
            keras.metrics.Precision(thresholds=0.9, class_id=0, name="precision90k"),
            RealPrecision(thresholds=0.9, class_id=0, name="precision90i"),
    ]
    """

    def __init__(self, name='real_precision', thresholds=0.5, class_id=0, class_weight=1, **kwargs):
        super(RealPrecision, self).__init__(name=name, **kwargs)
        self.true_positives = self.add_weight(name='tp', initializer='zeros')
        self.false_positives = self.add_weight(name='fp', initializer='zeros')
        self.class_id = class_id
        self.class_weight = class_weight
        self.thresholds = thresholds

    def update_state(self, y_true, y_pred, sample_weight=None):
        try:
            y_true_bools = tf.cast(y_true[:, self.class_id], tf.bool)
            class_scores = y_pred[:, self.class_id]
        except:
            y_true_bools = tf.cast(y_true, tf.bool)
            class_scores = tf.cast(y_pred, tf.float32)

        binary_predictions = tf.cast(class_scores >= self.thresholds, tf.bool)
        true_positives = tf.cast(tf.logical_and(binary_predictions, y_true_bools), self.dtype)
        false_positives = tf.cast(tf.logical_and(binary_predictions, tf.logical_not(y_true_bools)), self.dtype)

        if sample_weight is not None:
            sample_weight = tf.cast(sample_weight, self.dtype)
            true_positives = tf.multiply(true_positives, sample_weight)
            false_positives = tf.multiply(false_positives, sample_weight)

        true_positives_sum = tf.reduce_sum(true_positives)
        false_positives_sum = tf.reduce_sum(false_positives)

        self.true_positives.assign_add(true_positives_sum / self.class_weight)
        self.false_positives.assign_add(false_positives_sum)

    def result(self):
        precision = tf.math.divide_no_nan(self.true_positives, self.true_positives + self.false_positives)
        return precision

    def reset_states(self):
        # The state of the metric will be reset at the start of each epoch.
        self.true_positives.assign(0)
        self.false_positives.assign(0)

    def get_config(self):
        base_config = super().get_config()
        config = {"thresholds": self.thresholds, "class_id": self.class_id, "class_weight": self.class_weight}
        return {**base_config, **config}
