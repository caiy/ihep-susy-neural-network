import tensorflow as tf


def calZn(sig, nb, relativeBkgUncert):
    Err = relativeBkgUncert * nb
    E2 = Err * Err
    n = sig + nb
    dll = n * tf.math.log(((sig + nb) * (nb + E2)) / (nb * nb + (sig + nb) * E2)) - \
            ((nb * nb) / (E2)) * tf.math.log((nb * nb + (sig + nb) * E2) / (nb * (nb + E2)))
    nsigma = tf.sqrt(2. * dll)
    nsigma_without_nan = tf.where(tf.math.is_nan(nsigma), tf.zeros_like(nsigma), nsigma)
    return nsigma_without_nan
