# from base.utils import check_args
# TODO: how to use args

# TODO: add arguments
# change default value to -1
def HyperOpt(reader):
    debug = False
    if check_args('trial') == 0:
        # lephad met data: memory_usage_1job = 4 + 2t
        pop = []
        concurrency = 8
        num_trial_per_job = 1
        tot_num_trial = 200
        tot_jobs = ceil(tot_num_trial / num_trial_per_job)
        for i in range(tot_jobs):
            utils.holdon(pop, concurrency)
            log.logger.info(f'Submit {i+1}/{tot_jobs} processes')
            time.sleep(20)
            pop.append(subprocess.Popen(["python"] + sys.argv + ["-t", str(num_trial_per_job)]))
            if debug:
                return
        return

    import optuna
    # from optuna.trial import TrialState

    workdir = check_args('workdir')
    store_dir = check_args('store') + TIME_NOW
    log.logger.info("Set store folder: %s", store_dir)
    input_file = check_args('input')
    optim_tag = check_args('grid_search')
    file_handle = file_handler.FileHandler(input_file, reader.get_sample_config())

    nfold, cv_dataset, test_dataset = file_handle.get_nfold_train_test()
    kfold = KFold(n_splits=nfold, shuffle=False, random_state=None)

    def objective(trial):
        # the hyper-parameer optimization settings
        gs_reader = config_reader.GSConfigReader(reader)

        # optimizer
        # selected_optimizer = "Adam"  # trial.suggest_categorical("optimizer", ["Adam", "SGD"])
        # gs_reader.set_train_config("optimizer", selected_optimizer)
        # if selected_optimizer == "Adam":
        gs_reader.set_train_config(key1="optim_params", key2="learning_rate", value=trial.suggest_float("adam_learning_rate", 5e-6, 5e-2, log=True))
        # elif selected_optimizer == "SGD":
        #     gs_reader.set_config(key1="optim_params",key2="learning_rate",value=trial.suggest_float("sgd_learning_rate",1e-5, 5e-2, log=True))
        #     gs_reader.set_config(key1="optim_params",key2="momentum",value=trial.suggest_float("sgd_momentum",1e-5, 1e-1, log=True))

        selected_method = trial.suggest_categorical("method", ["class_weight", "under_sampling", "over_sampling"])  #,"focal"])
        if selected_method == "class_weight":
            gs_reader.set_sample_config("class_weight", [735, 1, 1, 1, 1, 1, 1, 1])
            pass
        elif selected_method == "under_sampling":
            # S/B raw numbe ratio from 0.1 to 0.9; keep physics factor and 735
            # under_sampling_ratio = trial.suggest_float("under_sampling", 0.1, 0.9 )
            under_sampling_factor = trial.suggest_float("under_sampling", 0.002, 0.2, log=True)  # 41698 / 2.25187e+06 / under_sampling_ratio

            gs_reader.set_sample_config("class_weight", [735 * under_sampling_factor, 1, 1, 1, 1, 1, 1, 1])
            gs_reader.set_sample_config(
                "sampling", {
                    "StauStauISR_100_70_Nom": 1,
                    "Higgs_Nom": under_sampling_factor,
                    "SingleTop_Nom": under_sampling_factor,
                    "OtherTop_Nom": under_sampling_factor,
                    "TopPair_Nom": under_sampling_factor,
                    "VV_Nom": under_sampling_factor,
                    "Wjets_Nom": under_sampling_factor,
                    "Zlljets_Nom": under_sampling_factor,
                    "Zttjets_Nom": under_sampling_factor
                })
        elif selected_method == "over_sampling":
            under_sampling_factor = trial.suggest_float("under_sampling", 0.003, 0.2, log=True)  # 41698 / 2.25187e+06 / under_sampling_ratio
            b_s_ratio = 735 * under_sampling_factor
            over_sampling_factor = trial.suggest_int("over_sampling", b_s_ratio * 0.5, b_s_ratio * 1.1)

            gs_reader.set_sample_config("class_weight", [735 * under_sampling_factor / over_sampling_factor, 1, 1, 1, 1, 1, 1, 1])
            gs_reader.set_sample_config(
                "sampling", {
                    "StauStauISR_100_70_Nom": over_sampling_factor,
                    "Higgs_Nom": under_sampling_factor,
                    "SingleTop_Nom": under_sampling_factor,
                    "OtherTop_Nom": under_sampling_factor,
                    "TopPair_Nom": under_sampling_factor,
                    "VV_Nom": under_sampling_factor,
                    "Wjets_Nom": under_sampling_factor,
                    "Zlljets_Nom": under_sampling_factor,
                    "Zttjets_Nom": under_sampling_factor
                })

        # architecture, next thing to be optimized
        # NN_depth = trial.suggest_int("NN_depth",3,6)
        # mlp_unit_list = [trial.suggest_int("layer0",128,512)] + [trial.suggest_int(f"layer{i+1}",64,512) for i in range(NN_depth-1)]
        # gs_reader.set_config("mlp_unit_list", mlp_unit_list)

        # avoid over-fitting
        # gs_reader.set_config("batchnorm", trial.suggest_categorical("batchnorm",[0,1,2]))
        # gs_reader.set_config("dropout", trial.suggest_float("dropout", 0.01, 0.99))
        # gs_reader.set_config("reg_lambda", trial.suggest_float("reg_lambda", 0.001, 0.99))

        # performance
        # gs_reader.set_config(key1="reducelr",key2="patience",value=trial.suggest_int("reduce_lr_patience",10,30))
        # gs_reader.set_config("batch_size", trial.suggest_categorical("batch_size",[128,256,512]))
        # gs_reader.set_config("activation", trial.suggest_categorical("activation",["relu","leaky_relu"]))
        # gs_reader.set_config("class_weight", trial.suggest_categorical("signal_weight", [734, 1]))

        targets = ["loss", "PR_AUC", "Zn", "Zn300"]
        outputs = []
        for ind, index in enumerate(kfold.split(cv_dataset)):
            store_path = f"{workdir}/{store_dir}/model{ind}"
            model = new_model(reader)
            training_history = model.fit(cv_dataset.iloc[index[0]], cv_dataset.iloc[index[1]], store_path)
            model.save(store_path)
            utils.SaveLossPlot(store_path, training_history)

            test_eval = model.evaluate(test_dataset)
            Zn = utils.compute_Zn(test_dataset, model, 0.3, 1 / file_handle.train_val_test_frac[2])
            Zn300 = utils.compute_Zn(test_dataset, model, 0.3, 300 / 140 / file_handle.train_val_test_frac[2])
            test_output = {"PR_AUC": test_eval["pr_auc_unsampled"], "loss": test_eval["loss"], "Zn": Zn, "Zn300": Zn300}
            # print(test_output)
            outputs.append(test_output)

        current, peak = tracemalloc.get_traced_memory()
        log.logger.info(f"No. {trial.number} trial finished, current memory [MB]: {current/1024/1024}, peak memory [MB]: {peak/1024/1024}")
        # print (outputs)
        means = [np.mean(list(fold_output[key] for fold_output in outputs)) for key in targets]
        return means

    study = optuna.create_study(
        # direction="minimize",
        directions=["minimize", "maximize", "maximize", "maximize"],
        study_name=f"optuna-study-{optim_tag}",
        storage=f"sqlite:///{workdir}/{optim_tag}.db.sqlite3",
        load_if_exists=True)
    # pruner=optuna.pruners.MedianPruner(n_startup_trials=10, n_warmup_steps=30, interval_steps=10))
    study.optimize(objective, n_trials=check_args('trial'))
