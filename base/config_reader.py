import json
from base.log import logger
import pprint
from copy import deepcopy
from base.utils import read_config


class ConfigReader:
    """
    A class to read, validate and store a JSON file {'include':[''],'sample_config':{...},'training_params':{...}}
    Attributes:
        sample_config (dict): A dictionary containing sample config parameters.
        train_config (dict): A dictionary containing training config parameters.
    """

    def __init__(self, config_file_name):
        """
        Initializes the ConfigReader by reading, checking, and storing configuration from a JSON file.
        Args:
            config_file_name (str): JSON file name.
        """
        if config_file_name:
            j_config = read_config(config_file_name)

            mandatory_keys = ['label_group', 'train_val_test_frac', 'nominal_features', 'weight_branch']
            self.sample_config = self.parse_config(j_config, 'sample_config', set(mandatory_keys))
            self.train_config = self.parse_config(j_config, 'training_params', {})

            logger.info("*" * 10 + "Sample Config" + "*" * 10 + "%s",
                        ''.join(['\n\tSConf: ' + k + ': ' + str(v) for k, v in self.sample_config.items()]))
            logger.info("*" * 10 + "Train Config" + "*" * 10 + "%s",
                        ''.join(['\n\tTConf: ' + k + ': ' + str(v) for k, v in self.train_config.items()]))
        else:
            raise ValueError(f"bad config file:{config_file_name}")

    def check_dict_key(self, target_dict, key):
        """
        Checks if the specified key is in the target dictionary and raises a RuntimeError if not.
        Args:
            target_dict (dict): The dict to be checked.
            key (str): The key to be checked in the dictionary.
        Raises:
            RuntimeError: If the key is not found in the target dictionary.
        """
        if key not in target_dict:
            logger.critical('Mandatory key "' + key + '" not found in config file')
            raise RuntimeError('config key not found')

    def parse_config(self, config, key_name, mandatory_keys):
        """
        Returns config[key_name], and checks if the mandatory keys are in config[key_name].
        Args:
            config (dict): The config dict {'sample_config':{...},'training_params':{...}}
            key_name (str): The key for the desired sub-config ('sample_config' or 'training_params').
            mandatory_keys (set of str): The keys must be present in the sub-config config[key_name].
        Raises:
            RuntimeError: If the key_name is not found in the main configuration or if any mandatory keys are missing.
        Returns:
            dict: config[key_name]
        """
        if key_name not in config:
            logger.critical('No "' + key_name + '" in the config file, please check!')
            raise RuntimeError('config key not found')
        next_level_config = config[key_name]
        for k in mandatory_keys:
            self.check_dict_key(next_level_config, k)
        return next_level_config

    def get_sample_config(self):
        """Returns a copy of sample config dictionary"""
        return deepcopy(self.sample_config)

    def get_train_config(self):
        """Returns a copy of train config dictionary"""
        return deepcopy(self.train_config)


class GSConfigReader(ConfigReader):
    """
    The class is copied from a normal ConfigReader and can be modified.
    """

    def __init__(self, ConfigReader):
        """
        The instance holds the deep copies of sample_config and train_config from input
        Args: 
            ConfigReader (ConfigReader)
        """
        self.sample_config = deepcopy(ConfigReader.sample_config)
        self.train_config = deepcopy(ConfigReader.train_config)

    def set_sample_config(self, key1, value, key2=None):
        """
        Sets a value in the sample configuration. It allows for both direct key-value pairs and nested configurations. 
        If a key from train_config is found in sample_config, it raises an error.

        Args:
            key1 (str): The primary key in the sample configuration.
            value (Any): The value to set for the primary key or the nested key.
            key2 (str, optional): A secondary key if the sample configuration is nested. Defaults to None.

        Raises:
            RuntimeError: If the primary key exists in train_config, an exception is raised to prevent key collisions.
        """
        if key2:
            self.sample_config[key1][key2] = value
        else:
            self.sample_config[key1] = value
        if key1 in self.train_config:
            raise RuntimeError('Sample config key found in train config: ', key1)

    def set_train_config(self, key1, value, key2=None):
        if key2:
            self.train_config[key1][key2] = value
        else:
            self.train_config[key1] = value
        if key1 in self.sample_config:
            raise RuntimeError('Train config key found in sample config: ', key1)
