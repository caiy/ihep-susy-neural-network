# **I**HEP **S**USY N**e**ural N**e**twork

**I**HEP **S**USY N**e**ural N**e**twork(ISee) is a script for helping to do NN training/predicting based on tf.Keras

## How to install cuda on GPU and run locally

Generally ISee could run on both CPU and GPU. But if a GPU is available, it's better to use the GPU training to speedup

If you have a GPU, please check the version of supported version and compatibility between _tensor_gpu_ version, _Python_ version, _cuDNN_ version, and _CUDA Toolkit_ version:

[Window](https://www.tensorflow.org/install/source_windows#gpu)

[Linux](https://www.tensorflow.org/install/source#gpu)

Generally, you can select a version you like, and install the **corresponding** version of CUDA(CUDA Toolkit), cuDNN, tensorflow-gpu. (**This relation is really important!**)


### Windows!
But maybe there are some additional check for **Windows**:

1. For windows, please check if the GPU support the Cuda : https://developer.nvidia.com/cuda-gpus#compute
2. Then, check which CUDA Toolkit version can support the current GPU (CUDA) Driver, you can update your GPU Driver version if it is not compatible.
3. Install CUDA Toolkit by .exe file.
4. Unzip cuDNN, copy the three folder to corresponding folder of CUDA toolkit you have just installed. For example: copy _bin/*_ of **cuDNN** to _bin/*_ of **CUDA Toolkit**.
5. Add these path to your environment variables
    > C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\<version>\bin

    > C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\<version>\libnvvp

    > C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\<version>\lib

    > C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\<version>\include

6. Install the tensorflow:
    > For windows, you can install conda and creat a virtual environment and install conda [Install conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html)

### Archlinux/Manjaro
Setup environment for ArchLinux/Manjaro is easy. Just use pacman/(pamac for Manjaro), install these packages and their dependencies. 
```
python-uproot, python-tensorflow-cuda, tensorflow-cuda, python-keras, python-scikit-learn, python-matplotlib, python-tabulate
```
For compatibility, in arch-based systems it's not recommended to use pip/conda but pacman/pamac to install python packages(the installed pip/conda python package 
could easily be outdated after a system update) while one could use `python venv` if you have version-specific requirements.


## How to install ISee on LXPLUS or lxslc

**0**,
Python3.9 or Python 3.10 is required.

Install Python3.9 wihout admin:
```
mkdir python
cd pyton
mkdir .local
wget https://www.python.org/ftp/python/3.9.16/Python-3.9.16.tgz
tar zxvf Python-3.9.16.tgz
cd Python-3.9.16
./configure --prefix=/YourPath/.local
make
make install
```

**1**, 
Create virtual env if you are using 3.10:
```
python -m venv mlenv
```
and activate it:
```
source mlenv/bin/activate
```

**2**, 
Upgrade pip and required package. 
```
python -m pip install --upgrade pip
```
The required package is: pandas, uproot(4), scikit-learn, tensorflow:
```
python -m pip install tensorflow[and-cuda]
python -m pip install pandas uproot awkward scikit-learn matplotlib tabulate tensorboard imblearn
python -m pip install shap optuna optuna-integration
```
If nightly tensorflow is needed, use :
```
python -m pip install tf-nightly[and-cuda]
```

**3**, Have a training!


## Command Line

`config/example.json` is the config file for input samples, input trees, training variables, weight filter.

`isee.py` is the main script for NN train and predict. It uses the tf.keras, a High-level api for neural network architecture.


```
python isee.py -h
usage: isee.py [-h] [-i INPUT] [-c CONFIG] [-p PROCESS] [-m MODEL] [-o OUTPUT] [-s STORE] [-w WORKDIR] [-v] [-g GRID_SEARCH]

IHEP SUSY Neural Network

options:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        The input root file for training/prediction
  -c CONFIG, --config CONFIG
                        The config file used in the program for training
  -p PROCESS, --process PROCESS
                        The process want to do, choose among "show", "train", "predict" and "all", "xtrain", "xpredict", "xall"
  -m MODEL, --model MODEL
                        The model structure used for the training. Choose one from the analysis folder
  -o OUTPUT, --output OUTPUT
                        The output file for show or prediction
  -s STORE, --store STORE
                        When training, it will be the dir to store the output. When predicting, it will be the input dir to read the model got from training
  -w WORKDIR, --workdir WORKDIR
                        Root directory for reading and writing.
  -v, --verbose         show verbose log messages
  -g GRID_SEARCH, --grid_search GRID_SEARCH
                        The config file used for grid search
```

## Config File

- sample_config

| keyword |  Description | Example |
| :--- | :--- | :--- |
| **mandatory** | |
| train_val_test_frac | The fraction of train, validation, test datasets. | [0.8, 0.1, 0.1] |
| label_group | A list of lists. Each nested list is a class. Regular expression is supported. | ["StauStauISR_100_70_Nom"], ["Higgs_Nom"], ["SingleTop_Nom","OtherTop_Nom"], ["TopPair_Nom"], ["VV_Nom"], ["Wjets_Nom"], ["Zlljets_Nom"], ["Zttjets_Nom"] |  
| nominal_features | List of nominal feature names. Should be the same as the branch names. | ["MTtau1", "MTtau2", "MTjet", "Minvtt", "MCTtt", "MT2tt_50", "MT2tt_130"] | 
| weight_branch | Branch name of physics weight | "Weight_mc"|
| **optional** |  | |
| class_weight | Extra weight for unbalanced class. Should be the long as label_group. | [735,1,1,1,1,1,1,1] |  
| normalization | normalize features before training | "standard" or "robust" | 
| weight_handle | How to handle the negative MC weight | 'drop_negative', 'min_positive', 'softplus', 'exponential'|
| sampling | A dictionary {tree_reg : sample_times}. Sample some trees more than 1 time. Not recommended. | {"Stau.*Nom":10,"VV_Nom":0.1} |  
| class_id | The label of signal class in multi-class classification | 0 | 
| random_seed | Random seed for sampling train, validation, test datasets. | 12345 |  
| max_weight | Events with phsics weight larger than max_weight are ignored. | 100 |  


- training_params

| keyword |  Description | Example |
| :---: | :---- | :--- |
| optimizer | Optimizer name. | "Adam" |
| optim_params | Dictionary of optimizer parameters. | {"learning_rate": 1e-5}| 
| activation | activation function name | "leaky_relu" |  
| batch_size | batch size | 256 |  
| epochs | epoch number | 50 |  
| batchnorm| whether add batch norm | 0 or 1 or 2|
| dropout | drop out rate | 0.1 |
| reg_lambda | L2 regularization | 0.01 |
|loss | loss function type | "Crossentropy" or "Focal" |
|loss_params | Dictionary of loss paramters. | {"alpha":[20,1,1,1,1,1,1,1], "gamma":2} |
| lr_scheduler| learning rate scheduler name | "time_based_decay" or "warmup_time_based_decay" |
| lrsc_params | Dictionary of lr scheduler | {'drop_rate': 0.99, 'epochs_drop': 1, "stop_factor":0.001} | 
| **callbacks**||
| earlystop | Keyword parameters for keras.callbacks.EarlyStopping(**Here**) | {'monitor':'val_loss', 'patience':50, 'start_from_epoch':50} |
| ckpt | Keyword parameters for keras.callbacks.ModelCheckpoint(filepath=workdir + "/ckpt/weights.e{epoch:02d}.l{val_loss:.4f}.hdf5", save_best_only=True, **Here**) |{'monitor':'val_loss', 'mode':'min', 'initial_value_threshold':0.1} |
| reducelr | Keyword parameters for keras.callbacks.ReduceLROnPlateau(**Here**) | {'monitor'='val_loss', 'factor'=0.2, 'patience'=5, 'min_lr'=0.0000001}  |
| epoch_pv | Whether Save ROC plot per epoch. | true / false | 
| **MLP parameter**||
| mlp_unit_list | List of neuron numbers. |  [128,256,128,128,128,64] |  
<!-- | num_head | Multi-head attention head number | 1 | -->
<!-- | dim_key | dimension of key/value. Suggestion: dim_key*num_head == particle feature number | 8 |  -->

## Metrics and Callbacks
The metrics to be evaluated during training should be set in `base_model.py`. Some metrics are only for binary classification, while some metrics are for both binary and categorical classification.

Callback classes of keras
```python
keras.callbacks.EarlyStopping
keras.callbacks.ModelCheckpoint
keras.callbacks.ReduceLROnPlateau
```

Configs for the callbacks
```json
"training_params": {
    "earlystop":{
      "monitor": "val_loss",
      "mode":"min",
      "patience":25,
      "start_from_epoch":10
    },
    "ckpt":{
      "monitor": "val_loss",
      "mode":"min"
    },
    "reducelr":{
      "monitor": "val_loss",
      "patience":10
    }
}
```

`keras.callbacks.LearningRateScheduler` needs a scheduler fucntion `scheduler(epoch, lr)` that returns next learning rate. For most cases, we should use adaptive learning rate methods.  
`PerformanceVisualizationCallback` draws ROC curves and computes AUC of ROC at each epoch end, and is very time-consuming.

## Notes
### About data
The events in tree is read out by `file_handler.read_ntuple()`, then decorated by `file_handler.get_all_sample_pd()` with **TreeName, label** as a `pd.DataFrame` (sampling, max weight).  
Then, the events are **sampled randomly** and splitted into several datasets by `file_handler.get_train_val_test(), file_handler.get_train_test(), file_handler.get_nfold_train_test()`.  
The datasets are transformed into features, weights and labels by `base_model.prepare_data_input` and **the class weight is applied in this step**.  
These arrays are used in `base_model.fit, predict, evaluate`. The `predict` is used to get scores in utils functions. The `evaluate` is used in cross-validation.  
**The class weight affects model training and cross-validation evaluation.**

## tensorboard
```
# remote 
tensorboard --logdir output/storexxx/tblog
# local
ssh -L 6006:localhost:6006 jiarong@atlas.sysuss.tech -p 6001
ssh -L 6006:localhost:6006 -J jiarong@atlas.sysuss.tech:6001 jiarong@172.25.0.55 -p 20022
```

## example code
```
# show yields
nohup python isee.py -i ../data/HH_met.root -p show -c config/MLP_0_HH_4.json &

# train & prediction
nohup python isee.py -i ../data/HH_met.root -p all -c config/MLP_0_HH_2.json  &
nohup python isee.py -i ../data/HH_met.root -p predict -c config/MLP_0_HH_3.json -s store-id &

# cross-validation
nohup python isee.py -i ../data/HH_met.root -p xval -c config/MLP_0_HH_3.json  &
nohup python isee.py -i ../data/HH_met.root -p xval -c config/MLP_0_HH_4.json &
```