from base import base_model, callbacks
import tensorflow as tf
import pandas as pd
from tensorflow import keras
from keras import layers, backend as K, regularizers
from collections import defaultdict


class MLPModel(base_model.BaseModel):

    def __init__(self, sample_config, train_config=None):
        super().__init__(sample_config, train_config)

    def build_model(self, nominal_input):
        """"
        below is to build your model
        Training params:
            mlp_unit_list
            batchnorm
            dropout
            activation
            reg_lambda
        """
        output = nominal_input

        mlp_unit_list = self.train_config['mlp_unit_list']
        BatchNorm = self.train_config['batchnorm']
        
        if not isinstance(self.dropout, list):
            if self.dropout is not None:
                self.dropout = [self.dropout] * len(mlp_unit_list)   

        for idx, unit in enumerate(mlp_unit_list):
            output = layers.Dense(
                units=unit,
                kernel_regularizer=regularizers.L2(self.reg_lambda),
                bias_regularizer=regularizers.L2(self.reg_lambda))(
                    output)
            if BatchNorm == 1:
                output = layers.BatchNormalization()(output)
            output = layers.Activation(self.activation)(output)
            if self.dropout is not None and idx < len(mlp_unit_list) and self.dropout[idx] > 0:
                output = layers.Dropout(self.dropout[idx])(output)

        return output
