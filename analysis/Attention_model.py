from base import base_model, config_reader, callbacks
import tensorflow as tf
from tensorflow import keras
from keras import layers, backend as K, regularizers
from collections import defaultdict
from base.layer import transformer


class AttentionModel(base_model.BaseModel):
    # TODO: to be modified

    def __init__(self, sample_config, train_config):
        reader = config_reader.ConfigReader()
        reader.check_dict_key(train_config, 'num_head')
        reader.check_dict_key(train_config, 'dim_key')
        super().__init__(sample_config, train_config)

    def build_model(self, nominal_input, particle_input):
        """"below is to build your model"""
        if particle_input is None:
            raise RuntimeError('No particle input for Attention model.')

        attention_output = transformer.SelfAttention(
            num_head=self.train_config['num_head'], dim_key=self.train_config['dim_key'], layer_norm=None, dropout=self.dropout)(
                particle_input)
        attention_flatten = layers.Flatten()(attention_output)

        output = layers.Concatenate()([nominal_input, attention_flatten])

        mlp_unit_list = self.train_config['mlp_unit_list']
        for idx, unit in enumerate(mlp_unit_list):
            output = layers.Dense(
                units=unit,
                activation=self.activation,
                kernel_regularizer=regularizers.L2(self.reg_lambda),
                bias_regularizer=regularizers.L2(self.reg_lambda))(
                    output)
            if self.dropout is not None and idx != len(mlp_unit_list) - 1:
                output = layers.Dropout(self.dropout)(output)

        return output
