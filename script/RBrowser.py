import streamlit as st
import os

st.set_page_config(layout="wide")
import pandas as pd
from collections import Counter
from itertools import product


def list_keys(directory):
    files = {fname.split('.')[-2]: os.path.join(directory, fname) for fname in os.listdir(directory) if "isee" in fname}
    return files


st.title('ISEE Result Browser')
# global: directory, RunInfo1, RunInfo2, HidePlot
directory = st.text_input('Output directory', value='output', placeholder='Input your result directory')
iRunInfo1 = ["Rpara", "SConf", "TConf"]
iRunInfo2 = ["TrainRes", "ValRes", "TestRes", "TrainCV", "ValCV", "TestCV"]

RunInfo1 = st.multiselect('Show configs', iRunInfo1, default=iRunInfo1)
RunInfo2 = st.multiselect('Show results', iRunInfo2, default=iRunInfo2)
HidePlot = st.text_input('Hide plots', placeholder='ignore_keyword1, ignore_keywrod2', value="accuracy, PR, FB")
HidePlot = [ip.strip() for ip in HidePlot.split(',')] if HidePlot else ["XXX"]  # default: "", False
width = st.number_input('Plot width', value=400)


def getConf():
    files = list_keys(directory)
    nfile = len(files)
    conf_lst = []
    for block, logf in files.items():
        conf_lst += [line for line in open(logf, 'r') if any(key in line for key in RunInfo1)]
    all_conf = set(conf_lst)
    conf_count = Counter(conf_lst)
    common_conf = {k for k, v in conf_count.items() if v == nfile}
    uncommon_conf = all_conf - common_conf
    return all_conf, common_conf, uncommon_conf


# global: all_conf, common_conf, uncommon_conf, conf_cuts, res_cuts, res_cuts_dict, nocommon
all_conf, common_conf, uncommon_conf = getConf()
conf_cuts = st.multiselect('Only see results with config:', sorted([c for c in uncommon_conf if 'json' not in c]))
res_cuts = st.multiselect('Result selections:', [l[0] + ":" + l[1] for l in product(["Val", "Test"], ["roc_auc"])])
res_cuts_dict = {rc: st.slider(rc, 0., 1.) for rc in res_cuts}
nocommon = st.checkbox('Hide common configs', value=True)


def showMain():
    files = list_keys(directory)
    for TIME, logf in files.items():
        info1, info2 = getInfo(logf)
        if all(cut in info1 for cut in conf_cuts) and goodres(res_cuts_dict, info2):
            showBox(TIME, logf)


def getInfo(logf):
    # return useful lines with keywords
    info1 = [line for line in open(logf, 'r') if any(type in line for type in RunInfo1)]
    info2 = [line for line in open(logf, 'r') if any(type in line for type in RunInfo2)]
    return info1, info2


def showBox(TIME, logf):
    with st.expander(TIME, True):
        showInfo(logf)
        showPlot(TIME)


def formatInfo(line):
    return [line.split(':')[0].strip(), line.split(':')[1].strip(), ''.join(line.split(':')[2:]).strip()]


def showInfo(logf):
    '''
    call showTable to show information from logfile 'logf'
    RunInfo1, RunInfo2: keyword lists
    nocommon: hide lines in common_conf if Ture
    '''
    info1 = [[line for line in open(logf, 'r') if type in line] for type in RunInfo1]
    if nocommon:
        info1 = [[formatInfo(line) for line in info if line not in common_conf] for info in info1]
    else:
        info1 = [[formatInfo(line) for line in info] for info in info1]
    info1 = [x for x in info1 if x != []]
    for info, col in zip(info1, st.columns(len(info1))):
        with col:
            showTable(info)

    info2 = [formatInfo(line) for line in open(logf, 'r') if any(type in line for type in RunInfo2)]
    showTable(info2)


def showTable(lst):
    # show a list [[column, index, values]] as a table
    df = pd.DataFrame(lst, columns=['col', 'row', 'value'])
    pt = pd.pivot_table(df, values='value', index='row', columns='col', aggfunc=lambda x: x)
    st.table(pt)


def showPlot(TIME):
    # show plots in folder '*TIME'
    pics = []
    for root, dirs, files in os.walk(directory):
        for f in files:
            if ".png" in f and TIME in root:
                if not any((ip in f) or (ip in root) for ip in HidePlot):
                    pics.append(os.path.join(root, f))
    if len(pics) == 0:
        return
    #w = int(1600/len(pics))
    pics.sort()
    st.image(pics, width=width, caption=pics)


def goodres(res_cuts_dict, info2):
    # larger is better
    for name, cut in res_cuts_dict.items():
        ds, mat = name.split(':')
        for info in info2:
            if ds in info and mat in info:
                if float(info.split(':')[-1].split('+-')[0]) < cut:
                    return False
    return True


showMain()
