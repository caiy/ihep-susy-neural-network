import csv

def calculate_class_weights(input_csv,tag="",importance_coeff = [1, 0.2, 0.2, 1, 1, 0.5, 0.5]):
    # Read data from CSV
    csv_path = input_csv
    with open(csv_path, 'r') as f:
        reader = csv.reader(f)
        rows = list(reader)
    
    # Extract process names and sum of weights
    processes = rows[1][1:]  # Skip the first element 'Processes'
    sum_weights = [float(w) for w in rows[2][1:]]  # Convert sum of weights to floats

    # Multi-class calculation (excluding the last 'Bkg' entry)
    multi_classes = processes[:-1]
    multi_class_weights = sum_weights[:-1]
    

    total_weight = sum(multi_class_weights)
    K = len(multi_classes)
    final_class_weight = []
    for i in range(K):
        base_weight = total_weight / (K * multi_class_weights[i])
        final_class_weight.append(base_weight * importance_coeff[i])
    
    # max_weight = max(final_class_weight)
    # final_class_weight = [v / max_weight * 10 for v in final_class_weight]

    # Binary case calculation (signal vs Background)
    binary_classes = [processes[0], "Background"]
    binary_class_weights = [sum_weights[0], sum_weights[-1]]  # First and last entries
    total_weight_binary = sum(binary_class_weights)
    binary_final_weights = [
        total_weight_binary / (2 * binary_class_weights[0]),
        total_weight_binary / (2 * binary_class_weights[1])
    ]

    print(input_csv,tag)
    print("Multi-class class weights:")
    for c, w in zip(multi_classes, final_class_weight):
        print(f"{c}: {w:.4f}")
    print(f"Weights: {[round(w,4) for w in final_class_weight]}")
    # print("\nBinary class weights:")
    # for c, w in zip(binary_classes, binary_final_weights):
    #     print(f"{c}: {w:.4f}")
    # print(f"Weights: {[round(w,4) for w in binary_final_weights+[binary_final_weights[-1]]*(len(multi_classes)-2)]}")
    print()
    print()
    with open(f"{tag}.txt", "w") as f:
        f.write(f"{input_csv}, {tag}\n")
        f.write("Multi-class class weights:\n")
        for c, w in zip(multi_classes, final_class_weight):
            f.write(f"{c}: {w:.4f}\n")
        f.write(f"Weights: {[round(w,4) for w in final_class_weight]}\n\n")

        # f.write("Binary class weights:\n")
        # for c, w in zip(binary_classes, binary_final_weights):
        #     f.write(f"{c}: {w:.4f}\n")
        # f.write(f"Weights: {[round(w,4) for w in binary_final_weights+[binary_final_weights[-1]]*(len(multi_classes)-2)]}\n\n")


if __name__ == "__main__":
    calculate_class_weights("../data/store0129-3.csv", "hh-tight",[1, 0.1, 0.1, 0.5, 1, 1, 0.5]) # VV, Wjets
    calculate_class_weights("../data/store0130-5.csv", "lh-tight",[1, 0.1, 0.1, 1,   1, 1, 0.5])   # Top, VV, Wjets
    
    # calculate_class_weights("../data/store0129-4.csv", "hh-loose",[1, 0.2, 0.2, 1, 1, 0.5, 0.5])
    # calculate_class_weights("../data/store0130-2.csv", "lh-base",[1, 0.2, 0.2, 1, 1, 1, 1])
    # calculate_class_weights("../data/store0130-3.csv", "lh-loose",[1, 0.2, 0.2, 1, 1, 1, 1])
    # calculate_class_weights("../data/store0130-4.csv", "lh-medium",[1, 0.2, 0.2, 1, 1, 1, 1])
    