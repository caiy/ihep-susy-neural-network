import os

# os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import subprocess
import argparse
import sys
from datetime import datetime
import pandas as pd
import numpy as np
import tracemalloc
from tabulate import tabulate
from sklearn.model_selection import KFold
from collections import defaultdict
from math import sqrt, ceil
import time
from multiprocessing import Pool
import os

from base import log, config_reader, predict_model, utils, file_handler, layer, metrics
from analysis import MLPModel  #, AttentionModel
from base.hyper_opt import HyperOpt

parser = argparse.ArgumentParser(description='IHEP SUSY Neural Network')
parser.add_argument('-i', '--input', action='store', help='The input root file for training/prediction')
parser.add_argument('-c', '--config', action='store', help='The config file used in the program for training')
parser.add_argument(
    '-p', '--process', action='store', help='The process want to do, choose among "show", "train", "predict" and "all", "xtrain", "xpredict", "xall"')
parser.add_argument('-m', '--model', action='store', help='The model structure used for the training. Choose one from the analysis folder', default='MLP')
parser.add_argument('-o', '--output', action='store', help='The output file for show or prediction', default='out')
parser.add_argument(
    '-s',
    '--store',
    action='store',
    help='When training, it will be the dir to store the output. When predicting, it will be the input '
    'dir to read the model got from training',
    default='store')
parser.add_argument('-w', '--workdir', action='store', help='Root directory for reading and writing.', default='./output/')
parser.add_argument('-v', '--verbose', action='store_true', help='show verbose log messages', default=False)
parser.add_argument('-g', '--grid_search', action='store', help='The name of the hyper-parameter study')
parser.add_argument(
    '-t',
    '--trial',
    type=int,
    action='store',
    help='The number of trial for hyper-parameter optimization.  If 0, the job only submit sub processes.',
    default=-1)
args = vars(parser.parse_args())
TIME_NOW = datetime.now().strftime("%m%d")

maxnum = 0
for item in os.listdir(args["workdir"]):
    if TIME_NOW in item and item.endswith(".log"):
        inum = int(item.replace(".log", "").split("-")[-1])
        if inum > maxnum:
            maxnum = inum
TIME_NOW = f"{TIME_NOW}-{maxnum+1}"

def check_args(arg_name):
    """ Checks if the given argument is present in the global `args` dictionary. """
    var = args[arg_name]
    if var is None:
        log.logger.critical('Arg Option "' + arg_name + '" is not given! Program exit')
        sys.exit(1)
    return var

def test_tf():
    """ Tests the TensorFlow GPU configuration and performs a basic matrix multiplication to verify the setup. """
    import tensorflow as tf
    physical_devices = tf.config.list_physical_devices('GPU')
    try:
        tf.config.experimental.set_memory_growth(physical_devices[0], True)
    except:
        pass
    log.logger.info("Num GPUs Available: %s", len(physical_devices))
    log.logger.info("Is Cuda Available: %s", tf.test.is_built_with_cuda())
    # tf.debugging.set_log_device_placement(True)
    # print(tf.test.gpu_device_name())
    try:
        a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
        b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
        c = tf.matmul(a, b)
    except Exception:
        log.logger.critical('tf test not passed. Please check your env')
        sys.exit(1)


def new_model(reader):
    """
    Creates and returns a new model based on the provided reader configuration.
    Notes:
        There could be sepcial config parameters for some models.
    """
    model_name = check_args('model')
    reader.check_dict_key(reader.train_config, 'mlp_unit_list')
    if model_name == 'MLP':
        return MLPModel(reader.get_sample_config(), reader.get_train_config())
    else:
        log.logger.critical(f'The {model_name} model is not available. Please check your args')
        sys.exit(1)
        #return AttentionModel(reader.get_sample_config(), reader.get_train_config())


def count_sample(reader):
    """ 
    Processes and counts samples based on the reader's configuration, logs the results, and stores them in a CSV file. 
    The function will be called in "show" process
    """
    workdir = check_args('workdir')
    output = check_args('store') + TIME_NOW
    input_file = check_args('input')

    file_handle = file_handler.FileHandler(input_file, reader.get_sample_config())
    tot_dataset = file_handle.get_all_sample_pd()

    classified_dict = {}
    weight_br = reader.get_sample_config()['weight_branch']
    unique_tree_names = tot_dataset['TreeName'].unique().tolist()
    for name in unique_tree_names:
        classified_dict[name] = tot_dataset.loc[tot_dataset['TreeName'] == name, weight_br]

    headers = ["Processes"]
    weights = ["Sum of Weight"]
    weights_pos = ["Sum of Positive Weights"]
    weights_neg = ["Sum of Negative Weights"]
    raw_ns = ["Raw Number"]
    sum_weights = 0
    sum_weights_pos = 0
    sum_weights_neg = 0
    sum_raw_ns = 0
    for label, weight in classified_dict.items():
        headers.append(str(label))
        weights.append(str(weight.sum()))
        weights_pos.append(str(weight[weight > 0].sum()))
        weights_neg.append(str(weight[weight < 0].sum()))
        raw_ns.append(str(weight.shape[0]))
        if "ISR" not in str(label):
            sum_weights += weight.sum()
            sum_weights_pos += weight[weight > 0].sum()
            sum_weights_neg += weight[weight < 0].sum()
            sum_raw_ns += weight.shape[0]
    headers.append("Bkg")
    weights.append(str(sum_weights))
    weights_pos.append(str(sum_weights_pos))
    weights_neg.append(str(sum_weights_neg))
    raw_ns.append(str(sum_raw_ns))

    log.logger.info("\n" + tabulate([weights, raw_ns, weights_pos, weights_neg], headers=headers))
    df = pd.DataFrame([headers, weights, raw_ns])
    df.to_csv(os.path.join(workdir, output + '.csv'), mode='a', index=False)

def cross_one_fold(reader, train_dataset, val_dataset, store_path):
    model = new_model(reader)
    training_history = model.fit(train_dataset, val_dataset, store_path)
    model.save(store_path)
    utils.SaveLossPlot(store_path + "/", training_history)
    
def cross_validation(reader):
    workdir = check_args('workdir')
    store_dir = check_args('store') + TIME_NOW
    out_dir = check_args('output') + TIME_NOW
    log.logger.info("Set store folder: %s", store_dir)
    log.logger.info("Set output folder: %s", out_dir)
    input_file = check_args('input')
    file_handle = file_handler.FileHandler(input_file, reader.get_sample_config())

    store_path = os.path.join(workdir,store_dir)
    out_path = os.path.join(workdir,out_dir)
    os.makedirs(store_path, exist_ok=True)
    os.makedirs(out_path, exist_ok=True)

    if check_args('trial') == -1:
        pop=[]
        nfold = file_handle.get_nfold()
        work_path = os.path.join(workdir,store_dir)
        for i in range(nfold):
            pop.append(subprocess.Popen(["python"] + sys.argv + ["-w", work_path, "-t", str(i), "-s", "model", "-o", "model_out"]))
            time.sleep(1)
        filtered_argv = [v for v in sys.argv if v not in ["-p","xval"]]
        pop.append(subprocess.Popen(["python"] + filtered_argv + ["-p", "xall", "-w", work_path, "-s", "final_model", "-o", "final_model_out"]))
    else:    
        # train
        nfold, cv_dataset, test_dataset = file_handle.get_nfold_train_test()
        normalized_datasets = utils.handle_normalization(
            datasets=[cv_dataset, test_dataset],
            features=file_handle.feature_list,
            normalization_type=reader.get_sample_config().get('normalization', None),
            store_dir=os.path.join(workdir, store_dir)
        )
        cv_dataset, test_dataset = normalized_datasets
        
        kfold = KFold(n_splits=nfold, shuffle=False, random_state=None)
        for ind, index in enumerate(kfold.split(cv_dataset)):
            if ind == check_args('trial'):
                log.logger.info(f"Cross-validation train with fold-{ind}: {store_path}")
                train_dataset = cv_dataset.iloc[index[0]]
                val_dataset = cv_dataset.iloc[index[1]]
                cross_one_fold(reader,train_dataset, val_dataset, store_path)
                break

        # predict
        ModelPath = utils.find_ckpt(store_path)
        pr = predict_model.PredictModel(reader.get_sample_config())
        pr.load_model(ModelPath, custom_objects={"RealAUC": metrics.RealAUC})
        log.logger.info("Read model stored in: %s", ModelPath)
        
        total_samples = file_handle.get_all_sample_pd()
        total_samples = utils.handle_normalization(
            datasets=[total_samples],
            features=file_handle.feature_list,
            normalization_type=reader.get_sample_config().get('normalization', None),
            store_dir=os.path.join(workdir, store_dir)
        )[0]
        pr.get_prediction(total_samples, out_path)
        
        results = {}
        results["train"] = pr.evaluate(train_dataset)
        results["validation"] = pr.evaluate(val_dataset)
        results["test"] = pr.evaluate(test_dataset)
        results["all"]  = pr.evaluate(total_samples)
        pd.DataFrame(results).to_csv(f'{out_path}/results.csv')
        
        datasets = [train_dataset, val_dataset, test_dataset]
        labels = ["Train", "Val", "Test"]
        utils.SaveAllScorePlot(os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)
        utils.SavePlot(utils.SaveSigScorePlot, out_path + "/", pr, datasets, labels, pr.class_id)
        utils.SavePlot(utils.SaveBkgScorePlot, out_path + "/", pr, datasets, labels, pr.class_id)
        utils.SavePlot(utils.SaveROCPlot, out_path + "/", pr, datasets, labels, pr.class_id)
        utils.SavePlot(utils.SavePRPlot, out_path + "/", pr, datasets, labels, pr.class_id)

        utils.evaluate_shap(
            pr_model=pr,
            train_dataset=datasets[0],
            test_dataset=datasets[-1],
            n_train=1000,
            n_test=200,
            # n_train=10, n_test=2,
            masker="Partition",
            class_id=pr.class_id,
            workdir=out_path,
            max_display=200)
    
    # score = {
    #     part: {key: [np.mean([result[key] for result in results
    #                          ]), np.std([result[key] for result in results])] for key in results[0].keys()
    #           } for part, results in kfold_results.items()
    # }
    # sorted_keys = sorted(score['train'].keys())

    # log.logger.info("CV Train results\n\t%s",
    #                 "\n\t".join([f'TrainCV: {key}: {score["train"][key][0]}+-{score["train"][key][1]}' for key in sorted_keys]))

    # log.logger.info("CV Val results\n\t%s",
    #                 "\n\t".join([f'ValCV: {key}: {score["validation"][key][0]}+-{score["validation"][key][1]}' for key in sorted_keys]))

    # log.logger.info("CV Test results\n\t%s", "\n\t".join([f'TestCV: {key}: {score["test"][key][0]}+-{score["test"][key][1]}' for key in sorted_keys]))


def train(reader, hasval):
    """
    Trains a machine learning model based on the provided configuration and handles the logging, saving, and plotting of the training results.

    Args:
        reader (ConfigReader): The ConfigReader instance holding configuration
        hasval (bool):  A boolean indicating whether validation data is available.
    """
    log.logger.info("*" * 10 + "Training Start" + "*" * 10)
    workdir = check_args('workdir')
    store_dir = check_args('store') + TIME_NOW
    log.logger.info("Set store folder: %s", store_dir)
    input_file = check_args('input')

    model = new_model(reader)
    file_handle = file_handler.FileHandler(input_file, reader.get_sample_config())
    if hasval:
        datasets = file_handle.get_train_val_test()
    else:
        datasets = file_handle.get_train_test()

    datasets = utils.handle_normalization(
        datasets=datasets,
        features=file_handle.feature_list,
        normalization_type=reader.get_sample_config().get('normalization', None),
        store_dir=workdir + '/' + store_dir
    )

    training_history = model.fit(datasets[0], datasets[1] if hasval else None, workdir + '/' + store_dir)

    keyws = ['Train Results', 'Validation Results', 'Test Results'] if hasval else ['Train Results', 'Test Results']
    results = {keyws[ind]: model.evaluate(ds) for ind, ds in enumerate(datasets)}
    for key, res in results.items():
        log.logger.info("\n\t" + "\n\t".join([f'{key}: {val_name}: {val}' for val_name, val in res.items()]))
    model.save(workdir + '/' + store_dir)
    utils.SaveLossPlot(workdir + '/' + store_dir + "/", training_history)


def predict(reader, isall, hasval):
    """
    Performs predictions using a pre-trained model and generates evaluation plots.

    Args:
        reader (ConfigReader): The ConfigReader instance holding configuration
        isall (bool): A boolean indicating whether in all process or just predict process
        hasval (bool): A boolean indicating whether validation data is available.
    """
    log.logger.info("*" * 10 + "Prediction Start" + "*" * 10)

    output_path = check_args('output') + TIME_NOW
    log.logger.info("Set output folder: %s", output_path)
    workdir = check_args('workdir')
    store_dir = check_args('store') + TIME_NOW if isall else check_args('store')

    if store_dir.endswith('.keras'):
        ModelPath = workdir + '/' + store_dir
    else:
        ModelPath = utils.find_ckpt(workdir + '/' + store_dir)
    log.logger.info("Read model stored in: %s", ModelPath)

    input_file = check_args('input')
    file_handle = file_handler.FileHandler(input_file, reader.get_sample_config())
    if hasval:
        datasets = file_handle.get_train_val_test(predict=True)# avoid negative weight handle, over/under-sampling 
    else:
        datasets = file_handle.get_train_test(predict=True)
    total_samples = file_handle.get_all_sample_pd()

    datasets = utils.handle_normalization(
        datasets=datasets,
        features=file_handle.feature_list,
        normalization_type=reader.get_sample_config().get('normalization', None),
        store_dir=workdir + '/' + store_dir
    )
    total_samples = utils.handle_normalization(
        datasets=[total_samples],
        features=file_handle.feature_list,
        normalization_type=reader.get_sample_config().get('normalization', None),
        store_dir=workdir + '/' + store_dir
    )[0]

    # produce predict root file
    pr = predict_model.PredictModel(reader.get_sample_config())
    pr.load_model(ModelPath, custom_objects={"RealAUC": metrics.RealAUC})
    pr.get_prediction(total_samples, os.path.join(workdir,output_path))
    
    labels = ["Train", "Val", "Test"] if len(datasets) == 3 else ["Train", "Test"]
    utils.SaveAllScorePlot(os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)
    utils.SavePlot(utils.SaveSigScorePlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)
    utils.SavePlot(utils.SaveBkgScorePlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)
    utils.SaveAllScorePlot(os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id,ylog=True)
    utils.SavePlot(utils.SaveSigScorePlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id,ylog=True)
    utils.SavePlot(utils.SaveBkgScorePlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id,ylog=True)
    utils.SavePlot(utils.SaveROCPlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)
    utils.SavePlot(utils.SavePRPlot, os.path.join(workdir,output_path), pr, datasets, labels, pr.class_id)

    utils.evaluate_shap(
        pr_model=pr,
        train_dataset=datasets[0],
        test_dataset=datasets[-1],
        n_train=1000,
        n_test=200,
        # n_train=100, n_test=40,
        masker="Partition",
        class_id=pr.class_id,
        workdir=workdir + '/' + output_path,
        max_display=200)


def main():

    tracemalloc.start()  # trace memory allocations
    verbose = args['verbose']
    os.makedirs(args['workdir'], exist_ok=True)
    log.init_logger(args['workdir'] + '/isee.' + TIME_NOW, verbose)
    log.logger.info("*" * 10 + "IHEP SUSY Neural Network Start" + "*" * 10)
    log.logger.info("*" * 10 + "Running parameters" + "*" * 10 + "%s", ''.join(["\n\tRpara: " + k + ": " + str(args[k]) + "," for k in args]))

    input_file = check_args('input')
    workdir = check_args('workdir')
    process = check_args('process')
    config_file_name = check_args('config')
    reader = config_reader.ConfigReader(config_file_name)
    store_dir = check_args('store')

    # test if tf-gpu is valid
    test_tf()

    if process == 'show':
        count_sample(reader)

    if process == "hpo":
        HyperOpt(reader)

    if process == 'train' or process == 'all':
        train(reader, True)

    if process == 'predict' or process == 'all':
        predict(reader, process == 'all', True)

    if process == 'xval':
        cross_validation(reader)

    if process == 'xtrain' or process == 'xall':
        train(reader, False)

    if process == 'xpredict' or process == 'xall':
        predict(reader, process == 'xall', False)

    _, peak = tracemalloc.get_traced_memory()
    log.logger.info("Peak memory [MB]: %s", peak / 1024 / 1024)
    tracemalloc.stop()
    log.logger.info("*" * 10 + "IHEP SUSY Neural Network End" + "*" * 10)


if __name__ == "__main__":
    #os.makedirs('./gpumonitor', exist_ok=True)
    #pid = os.getpid()
    #glog = open('./gpumonitor/' + str(pid) + '.log', 'a')
    #nvidia_monitor = subprocess.Popen(["nvidia-smi", "-l", "120"], stdout=glog, stderr=glog)
    main()
    #nvidia_monitor.kill()
